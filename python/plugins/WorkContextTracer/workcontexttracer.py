# -*- coding: utf-8 -*-
"""
/***************************************************************************
 WorkContextTracer
                                 A QGIS plugin
 WorkContextTracer
                              -------------------
        begin                : 2016-08-25
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from itertools import groupby

from PyQt4.QtCore import pyqtSignal, QObject

from qgis.core import QGis, QgsProject, QgsMapLayerRegistry, QgsMapLayer, QgsLogger, QgsDataSourceURI, QgsFeature, QgsFeatureRequest
from qgis.gui import QgsMessageBar
from qgis.utils import iface

import config as wcc

class SingletonType(type):
    def __call__(cls, *args, **kwargs):
        try:
            return cls.__instance
        except AttributeError:
            cls.__instance = super(SingletonType, cls).__call__(*args, **kwargs)
            return cls.__instance

class WorkContextTracer:
    __metaclass__ = SingletonType

    def __init__(self):
        self.signaler = Signaler()
        self.workContextChanged = self.signaler.workContextChanged
        self.settingsChanged = self.signaler.settingsChanged

    def getWorkContextLayer(self):
        return self._getLayerByTableName(wcc.WORK_CONTEXT_TABLE_NAME)
    
    def getWorkContextItemLayer(self):
        return self._getLayerByTableName(wcc.WORK_CONTEXT_ITEM_TABLE_NAME)


    def activeWorkContext(self):
        return QgsProject.instance().readEntry(wcc.WORK_CONTEXT_SCOPE, wcc.WORK_CONTEXT_KEY)[0]
    
    def isActiveWorkContext(self):
        return self.activeWorkContext() != ""
    
    def createWorkContext(self):

        workContextLayer = self.getWorkContextLayer()
        
        if not workContextLayer.isEditable():
            workContextLayer.startEditing()

        result = iface.vectorLayerTools().addFeature(workContextLayer)
        
        QgsLogger.debug(str(result))
        
        workContextLayer.commitChanges()
        
        # jesli nadal jest edytowalna, to znaczy ze nie udalo sie zatwierdzic zmian, wiec wycofuje
        if workContextLayer.isEditable():
            from workcontexttracerplugin import WorkContextTracerPlugin
            iface.messageBar().popWidget ()
            workContextLayer.rollBack()
            if result[1].attributes()[0] != None:
                iface.messageBar().pushMessage(WorkContextTracerPlugin.tr(u"Context with specified name already exists"), QgsMessageBar.WARNING, 5)
            else:
                iface.messageBar().pushMessage(WorkContextTracerPlugin.tr(u"Context name cannot be empty"), QgsMessageBar.WARNING, 5)
    
    def openWorkContext(self, workContext):
        workContextName = workContext.attribute(wcc.WC_NAME)
        QgsLogger.debug("open " + workContextName)
        
        if self.isActiveWorkContext():
            self.closeWorkContext(None)        

        QgsProject.instance().writeEntry(wcc.WORK_CONTEXT_SCOPE, wcc.WORK_CONTEXT_KEY, workContextName)
        
        self.signaler.workContextChanged.emit(workContextName)

        
    def closeWorkContext(self, workContext):
        if self.isActiveWorkContext():
            QgsProject.instance().writeEntry(wcc.WORK_CONTEXT_SCOPE, wcc.WORK_CONTEXT_KEY, None)
            
            self.signaler.workContextChanged.emit("")

            # TODO: zakonczyc edycje warstw 

    def addWorkContextItems(self, layerId, oids, operation):
        QgsLogger.debug("addWorkContextItems " + str(layerId) + " " + str(oids))
        
        try:
            if len(oids) < 1:
                return
            
            provider = self.getWorkContextItemLayer().dataProvider()
            
            className = layerId
            workContext = self.activeWorkContext()
            
            for oid in oids:
                feature = QgsFeature()
                feature.setAttributes([provider.defaultValue(0), className, oid, workContext, operation])
                if not provider.addFeatures([feature])[0]:
                    QgsLogger.debug("addWorkContextItem error {0} {1} {2} {3}".format(className, oid, operation, workContext))
                    iface.messageBar().popWidget()

        except Exception as ex:
            QgsLogger.debug("addWorkContextItems error")
            QgsLogger.debug(str(type(ex)))
            QgsLogger.debug(str(ex))
        
        QgsLogger.debug("addWorkContextItems DONE")
        
    def getLayerSettings(self):
        return QgsProject.instance().readListEntry(wcc.WORK_CONTEXT_SCOPE, wcc.WORK_CONTEXT_LAYERS)[0]
         
    def saveLayerSettings(self, settings):
        QgsProject.instance().writeEntry(wcc.WORK_CONTEXT_SCOPE, wcc.WORK_CONTEXT_LAYERS, settings) 
        self.signaler.settingsChanged.emit()   
        
    def zoomToWorkContext(self, workContextName):
        
        extent = None
        
        contextLayer = self.getWorkContextItemLayer()
        
        req = QgsFeatureRequest()
        req.setFilterExpression(u'"{0}" = \'{1}\''.format(wcc.WC_NAME, workContextName))
        req.addOrderBy(wcc.WCI_CLASS)
        
        classIdx = contextLayer.fieldNameIndex(wcc.WCI_CLASS)
        idIdx = contextLayer.fieldNameIndex(wcc.WCI_ID)

        features = []
        
        for feature in contextLayer.getFeatures(req):
            attributes = feature.attributes()

            features.append([attributes[classIdx], attributes[idIdx]])
       
        for table, layerFeatures in groupby(features, key = lambda x: x[0]):
            
            QgsLogger.debug(table)
            try:
                layer = QgsMapLayerRegistry.instance().mapLayers()[table]
            except KeyError:
                layer = self._getLayerByTableName(table)
            if layer is not None and layer.geometryType() != QGis.NoGeometry:

                oids = list(set([t[1] for t in layerFeatures]))
                        
                req = QgsFeatureRequest()
                req.setFilterExpression(u'"{0}" IN ({1})'.format(wcc.ID_ATTR, ",".join([str(k) for k in oids])))
                for feature in layer.getFeatures(req):
                    
                    if feature.geometry() is not None:
                        try:
                            extent.combineExtentWith(feature.geometry().boundingBox())
                        except AttributeError:
                            extent = feature.geometry().boundingBox()
        
        if extent is not None:
            iface.mapCanvas().setExtent(extent)
            iface.mapCanvas().refresh()    
    
    def _getLayerByTableName(self, table):
        layers = [lyr for lyr in QgsMapLayerRegistry.instance().mapLayers().values()\
                   if lyr.type() == QgsMapLayer.VectorLayer\
                   and QgsDataSourceURI(lyr.dataProvider().dataSourceUri()).table() == table]
        if len(layers) == 1:
            return layers[0]
        else:
            QgsLogger.debug("getLayerByTableName failed: " + table)
            return None
        
class Signaler(QObject):
    
    workContextChanged = pyqtSignal('QString')
    settingsChanged = pyqtSignal()
    
    def __init__(self):
        super(Signaler, self).__init__()
