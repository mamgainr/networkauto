# -*- coding: utf-8 -*-
"""
/***************************************************************************
 WorkContextTracer
                                 A QGIS plugin
 WorkContextTracer
                             -------------------
        begin                : 2016-08-25
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from qgis.core import QgsMapLayer, QgsMapLayerRegistry

from PyQt4 import QtGui, uic
from PyQt4.QtCore import QAbstractTableModel, Qt
from PyQt4.QtGui import QSortFilterProxyModel

from workcontexttracer import WorkContextTracer
import config as wcc

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'ui', 'workcontextoptions_dialog_base.ui'))


class WorkContextOptionsDialog(QtGui.QDialog, FORM_CLASS):
    
    def __init__(self, parent = None):
        """Constructor."""
        super(WorkContextOptionsDialog, self).__init__(parent)

        self.setupUi(self)

        self.model = None
        self.sortModel = None

        self.wct = WorkContextTracer()
        

    def loadSettings(self):
        layers = [l for l in QgsMapLayerRegistry.instance().mapLayers().values() \
                  if l.type() == QgsMapLayer.VectorLayer and l.providerType() != "memory" \
                  and l.fieldNameIndex(wcc.ID_ATTR) != -1 and l != self.wct.getWorkContextItemLayer()]

        settings = self.wct.getLayerSettings()

        self.model = LayerOptionModel(layers, {l.id():l.id() in settings for l in layers} , self)
        self.proxyModel = QSortFilterProxyModel(self);
        self.proxyModel.setSourceModel(self.model);
        
        self.layerTableView.setModel(self.proxyModel)
        self.layerTableView.resizeColumnsToContents()
        self.layerTableView.setSortingEnabled(True)
    
    def accept(self):
        self.wct.saveLayerSettings(self.model.getSettings())
        self.hide()    

class LayerOptionModel(QAbstractTableModel):        

    def __init__(self, layers, settings, parent = None):
        super(LayerOptionModel, self).__init__(parent)

        self._headerData = ["id", "name", ""]
        self.layers = layers
        self.settings = settings
        
    def rowCount(self, parent): 
        return len(self.layers) 

    def columnCount(self, parent): 
        return len(self._headerData)
 
    def data(self, index, role): 
        if not index.isValid(): 
            return None
                      
        if role == Qt.DisplayRole :
            
            if index.column() == 0:
                return self.layers[index.row()].id()
            elif index.column() == 1:
                return self.layers[index.row()].name()
            else: 
                return None
    
        if role == Qt.CheckStateRole:
            
            if index.column() == 2:
                return Qt.Checked if self.settings[self.layers[index.row()].id()] else Qt.Unchecked
            else: 
                return None
            
    def setData(self, index, value, role): 
        if not index.isValid(): 
            return None

        if index.column() == 2:
            self.settings[self.layers[index.row()].id()] = (value == Qt.Checked)

        self.dataChanged.emit(index, index)
            
        return True
         
    def flags(self, index):
        if not index.isValid():
            return Qt.NoItemFlags
        
        if index.column() == 2:
            return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsUserCheckable
        else:
            return Qt.ItemIsSelectable | Qt.ItemIsEnabled     
            
    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self._headerData[col]

    def getSettings(self):
        return [k for (k, v) in self.settings.iteritems() if v]
