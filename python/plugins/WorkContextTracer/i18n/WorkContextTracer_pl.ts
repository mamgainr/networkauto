<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="pl_PL" sourcelanguage="en">
<context>
    <name>WorkContextOptionDialogBase</name>
    <message>
        <location filename="workcontextoptions_dialog_base.ui" line="20"/>
        <source>Work Context Tracer Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkContextTracerDialog</name>
    <message>
        <location filename="workcontexttracer_dialog.py" line="76"/>
        <source>Work Context layer is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="workcontexttracer_dialog.py" line="92"/>
        <source>Invalid selection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkContextTracerPlugin</name>
    <message>
        <location filename="workcontexttracerplugin.py" line="217"/>
        <source>&amp;WorkContextTracer</source>
        <translation>Menadżer kontekstu</translation>
    </message>
    <message>
        <location filename="workcontexttracer.py" line="80"/>
        <source>Context with specified name already exists</source>
        <translation>Zlecenie o podanej nazwie juz istnieje</translation>
    </message>
    <message>
        <location filename="workcontexttracer.py" line="82"/>
        <source>Context name cannot be empty</source>
        <translation>Nazwa zlecenia nie może być pusta</translation>
    </message>
    <message>
        <location filename="workcontexttracerplugin.py" line="335"/>
        <source>Cannot perform operation, no context available</source>
        <translation>Nie można wykonać operacji, brak otwartego zlecenia</translation>
    </message>
    <message>
        <location filename="workcontexttracerplugin.py" line="187"/>
        <source>&amp;GenerateReport</source>
        <translation>Generuj raport</translation>
    </message>
    <message>
        <location filename="workcontexttracerplugin.py" line="248"/>
        <source>missing library </source>
        <translation>Brakująca biblioteka </translation>
    </message>
    <message>
        <location filename="workcontexttracerplugin.py" line="194"/>
        <source>&amp;Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkOrderMgrDialogBase</name>
    <message>
        <location filename="workcontextmgr_dialog_base.ui" line="37"/>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <location filename="workcontextmgr_dialog_base.ui" line="44"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="workcontextmgr_dialog_base.ui" line="30"/>
        <source>New</source>
        <translation>Nowy</translation>
    </message>
    <message>
        <location filename="workcontextmgr_dialog_base.ui" line="20"/>
        <source>Work Context Tracer</source>
        <translation>Menadżer kontekstu</translation>
    </message>
    <message>
        <location filename="workcontextmgr_dialog_base.ui" line="68"/>
        <source>Search:</source>
        <translation>Wyszukaj:</translation>
    </message>
</context>
</TS>
