# -*- coding: utf-8 -*-
"""
/***************************************************************************
 WorkContextTracer
                                 A QGIS plugin
 WorkContextTracer
                              -------------------
        begin                : 2016-08-25
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os, sys, tempfile, subprocess, datetime
from functools import partial
from itertools import groupby

from qgis.core import QgsLogger, QgsFeatureRequest, QgsMapLayerRegistry, QGis, NULL

from PyQt4.QtCore import QCoreApplication

from openpyxl import Workbook

from workcontexttracer import WorkContextTracer
import config as wcc

class WorkContextReport:
    

    @staticmethod
    def genenerateReport(workContextName):

        from workcontexttracerplugin import WorkContextTracerPlugin

        workContextLabel = "".join(x for x in workContextName if x.isalnum())
        
        wb = Workbook()
        
        wb.worksheets[0].title = workContextLabel

        WorkContextReport.writeReportPart(wb.worksheets[0], workContextName, "vw_ftth_planner_report")

        wb.create_sheet(WorkContextTracerPlugin.tr("serving_area"))
        WorkContextReport.writeReportPart(wb.worksheets[1], workContextName, "vw_ftth_planner_report_location")

        wb.create_sheet(WorkContextTracerPlugin.tr("cost_distribution"))
        WorkContextReport.writeReportPart(wb.worksheets[2], workContextName, "vw_ftth_planner_report_distribution")

        path = os.path.join(tempfile.gettempdir(), '{0}-{1}.xlsx'.format(workContextLabel, datetime.datetime.now().strftime('%Y%m%d%H%M%S')))
        QgsLogger.debug(str(path))

        wb.template = False
        wb.save(path)
        
        app = QCoreApplication.instance()
        if app is not None:
            app.aboutToQuit.connect(partial(os.remove, path))
        
        if sys.platform == "win32":
            os.system("start " + path)
        else:
            subprocess.call(["xdg-open", path])
            
    @staticmethod        
    def writeReportPart(worksheet, workContextName, viewName):

        reportLayer =  WorkContextTracer()._getLayerByTableName(viewName)
        fields = reportLayer.fields()
        
        req = QgsFeatureRequest()
        req.setFilterExpression("{0} = '{1}'".format(fields[1].name(), workContextName))
        
        for i in range(1, len(fields)):
            worksheet.cell(row = 1 , column = i , value = fields[i].displayName())
        
        for i, feature in enumerate(reportLayer.getFeatures(req), 2):

            row = feature.attributes()

            for j in range(1, len(fields)):
            
                cellValue = u''
                try:
                    cellValue = unicode(row[j]) if row[j] != NULL else u''
                    cellValue = float(row[j])
                except Exception:
                    pass
                
                worksheet.cell(row = i, column = j , value = cellValue)