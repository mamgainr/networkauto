# -*- coding: utf-8 -*-
"""
/***************************************************************************
 WorkContextTracer
                                 A QGIS plugin
 WorkContextTracer
                              -------------------
        begin                : 2016-08-25
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import os.path
from functools import partial

from qgis.core import QgsLogger, QgsMapLayerRegistry, QgsFeatureRequest, QgsFeature, QgsVectorLayer
from qgis.gui import QgsMessageBar
from qgis.utils import iface

from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, Qt
from PyQt4.QtGui import QAction, QIcon, QLineEdit, QApplication, QCursor

import resources

from workcontexttracer_dialog import WorkContextTracerDialog
from workcontextoptions_dialog import WorkContextOptionsDialog
from workcontexttracer import WorkContextTracer
import config as wcc

class WorkContextTracerPlugin:
    """ QGIS Plugin Implementation. """

    def __init__(self, iface):
        """ Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """

        self.iface = iface

        self.plugin_dir = os.path.dirname(__file__)

        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'WorkContextTracer_{}.qm'.format(locale))
 
        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        self.dlg = WorkContextTracerDialog(self.iface.mainWindow())
        self.optionsDlg = WorkContextOptionsDialog(self.iface.mainWindow())
        
        self.currentContextWidget = QLineEdit()
        self.currentContextWidget.setReadOnly(True)
        self.currentContextWidget.setMaximumWidth(200)

        self.actions = []
        self.menu = self.tr(u'&WorkContextTracer')
        self.toolbar = self.iface.addToolBar(u'WorkContextTracer')
        self.toolbar.setObjectName(u'WorkContextTracer')
        
        self.partials = {}
    
    @staticmethod
    def tr(message):
        """ Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        return QCoreApplication.translate('WorkContextTracerPlugin', message)

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag = True,
        add_to_menu = True,
        add_to_toolbar = True,
        status_tip = None,
        whats_this = None,
        parent = None):
        """ Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """ Create the menu entries and toolbar icons inside the QGIS GUI. """

        iconPathPrefix = ':/plugins/WorkContextTracer/icons/'

        iconPath = 'icon.svg'
        self.add_action(
            iconPathPrefix + iconPath,
            text = self.tr(u'&WorkContextTracer'),
            callback = self.showWorkContextDialog,
            parent = self.iface.mainWindow())
        
        self.actions.append(self.toolbar.addWidget(self.currentContextWidget))

        iconPath = 'report.svg'
        self.reportAction = self.add_action(
            iconPathPrefix + iconPath,
            text = self.tr(u'&GenerateReport'),
            callback = self.generateReport,
            parent = self.iface.mainWindow())
        
        iconPath = 'options.svg'
        self.add_action(
            iconPathPrefix + iconPath,
            text = self.tr(u'&Options'),
            callback = self.showOptionsDialog,
            parent = self.iface.mainWindow())
        
        iconPath = 'zoom.svg'
        self.zoomAction = self.add_action(
            iconPathPrefix + iconPath,
            text = self.tr(u'&ZoomTo'),
            callback = self.zoomToWorkConetext,
            parent = self.iface.mainWindow())
        
        QgsMapLayerRegistry.instance().layersAdded.connect(self.onLayersAdded)
        self.connectLayersEvents(QgsMapLayerRegistry.instance().mapLayers().values())
        
        self.iface.projectRead.connect(self.onWorkContextChanged)
        WorkContextTracer().workContextChanged.connect(self.onWorkContextChanged)
        WorkContextTracer().settingsChanged.connect(self.onSettingsChanged)
        
    def unload(self):
        """ Remove the plugin menu item and icon from QGIS GUI. """
        
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&WorkContextTracer'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar
        
        QgsMapLayerRegistry.instance().layersAdded.disconnect(self.onLayersAdded)
        self.disconnectLayersEvents(QgsMapLayerRegistry.instance().mapLayers().values())

        self.iface.projectRead.disconnect(self.onWorkContextChanged)
        WorkContextTracer().workContextChanged.disconnect(self.onWorkContextChanged)
        WorkContextTracer().settingsChanged.disconnect(self.onSettingsChanged)

    def showWorkContextDialog(self):
        """ Show work context dialog. """

        self.dlg.show()
        self.dlg.activateWindow()
        
    def generateReport(self):
        """ Generate current work context report. """
        
        if WorkContextTracer().isActiveWorkContext():
            try:
                QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
                from workcontextreport import WorkContextReport
                WorkContextReport.genenerateReport(WorkContextTracer().activeWorkContext())
            except ImportError as ex:
                iface.messageBar().pushMessage(self.tr(u"missing library ") + str(ex), QgsMessageBar.WARNING, 3)
            finally:
                QApplication.restoreOverrideCursor()
        else:
            iface.messageBar().pushMessage(self.tr(u"Cannot perform operation, no context available"), QgsMessageBar.WARNING, 3)

    def showOptionsDialog(self):
        """ Show work context options dialog. """

        self.optionsDlg.loadSettings()
        self.optionsDlg.show()
        self.optionsDlg.activateWindow()

    def zoomToWorkConetext(self):
        """ Zoom to work context items. """

        WorkContextTracer().zoomToWorkContext(WorkContextTracer().activeWorkContext())

    def onLayersAdded(self, layers):
        """ Connect layers signals when layer is added. """
        
        self.connectLayersEvents(layers)

    def connectLayersEvents(self, layers):
        """ Connect layers signals. """ 
        
        self.connectedLayers = WorkContextTracer().getLayerSettings()
        
        for lyr in [l for l in layers if type(l) == QgsVectorLayer and l.id() in self.connectedLayers]:
            
            self.partials[lyr.id()] = partial(self.checkActiveWorkOrder, lyr)
            
            lyr.editingStarted.connect(self.partials[lyr.id()])
            
            lyr.committedFeaturesAdded.connect(self.onFeaturesAdded)
            lyr.committedFeaturesRemoved.connect(self.onFeaturesRemoved)
            lyr.committedAttributeValuesChanges.connect(self.onAttributeValuesChanged)
            lyr.committedGeometriesChanges.connect(self.onGeometriesChanged)

    def onFeaturesAdded(self, layerId, features):
        """ Add work context item when features are added. """ 
        
        QgsLogger.debug("onFeaturesAdded " + str(layerId) + " " + str(features))
        QgsLogger.debug("onFeaturesAdded " + str([f.id() for f in features]))
        
        layer = QgsMapLayerRegistry.instance().mapLayer(layerId)
        idIdx = layer.fieldNameIndex(wcc.ID_ATTR)
        
        if len(features) > 0:
            WorkContextTracer().addWorkContextItems(layerId, [f.id() for f in features], "ADD")

    def onFeaturesRemoved(self, layerId, fids):
        """ Add work context item when features are removed. """ 
        
        QgsLogger.debug("onFeaturesRemoved " + str(layerId) + " " + str(fids))
        if len(fids) > 0:
            WorkContextTracer().addWorkContextItems(layerId, fids, "REMOVE")
        
    def onAttributeValuesChanged(self, layerId, attributesValues):
        """ Add work context item when features are changed. """ 
        
        QgsLogger.debug("onAttributeValuesChanged " + str(layerId) + " " + str(attributesValues))
        if len(attributesValues) > 0:
            WorkContextTracer().addWorkContextItems(layerId, attributesValues.keys(), "CHANGE")
        
    def onGeometriesChanged(self, layerId, geometries):
        """ Add work context item when geometries are changed. """ 
        
        QgsLogger.debug("onGeometriesChanged " + str(layerId) + " " + str(geometries))
        if len(geometries) > 0:
            WorkContextTracer().addWorkContextItems(layerId,  geometries.keys(), "MOVE")
        
    def checkActiveWorkOrder(self, layer):
        """ Check active work context when layer editing starts. """ 
        
        if not WorkContextTracer().isActiveWorkContext():
            iface.messageBar().pushMessage(self.tr(u"Cannot perform operation, no context available"), QgsMessageBar.WARNING, 3)
            layer.rollBack()

    def disconnectLayersEvents(self, layers):
        """ Disconnect layers signals. """ 
        
        for lyr in [l for l in layers if type(l) == QgsVectorLayer and l.id() in self.connectedLayers]:

            lyr.editingStarted.disconnect(self.partials[lyr.id()])

            lyr.committedFeaturesAdded.disconnect(self.onFeaturesAdded)
            lyr.committedFeaturesRemoved.disconnect(self.onFeaturesRemoved)
            lyr.committedAttributeValuesChanges.disconnect(self.onAttributeValuesChanged)
            lyr.committedGeometriesChanges.disconnect(self.onGeometriesChanged)


    def onWorkContextChanged(self):
        """ Update active work context label on toolbar. """
        
        self.currentContextWidget.setText(WorkContextTracer().activeWorkContext())
        
        self.reportAction.setEnabled(WorkContextTracer().isActiveWorkContext())
        self.zoomAction.setEnabled(WorkContextTracer().isActiveWorkContext())

    def onSettingsChanged(self):
        """ Update signals connections according to settings. """
        
        layers = QgsMapLayerRegistry.instance().mapLayers().values()
        self.disconnectLayersEvents(layers)
        self.connectLayersEvents(layers)
        
