# -*- coding: utf-8 -*-
"""
/***************************************************************************
 WorkContextTracer
                                 A QGIS plugin
 WorkContextTracer
                             -------------------
        begin                : 2016-08-25
        copyright            : (C) 2016 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load WorkOrderMgrPlugin class from file WorkOrderMgr.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from workcontexttracerplugin import WorkContextTracerPlugin
    return WorkContextTracerPlugin(iface)
