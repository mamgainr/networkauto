# -*- coding: utf-8 -*-
"""
/***************************************************************************
 WorkContextTracer
                                 A QGIS plugin
 WorkContextTracer
                             -------------------
        begin                : 2016-08-25
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from qgis.core import QgsVectorLayerCache, QgsFeature, QgsFeatureRequest
from qgis.gui import QgsAttributeTableModel, QgsAttributeTableFilterModel, QgsMessageBar
from qgis.utils import iface

from PyQt4 import QtGui, uic

from workcontexttracer import WorkContextTracer
import config as wcc

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'ui', 'workcontextmgr_dialog_base.ui'))


class WorkContextTracerDialog(QtGui.QDialog, FORM_CLASS):
    
    def __init__(self, parent = None):
        """Constructor."""
        super(WorkContextTracerDialog, self).__init__(parent)

        self.setupUi(self)

        self.model = None
        self.sortModel = None
        self.cache = None
        
        self.woNewButton.clicked.connect(self.newWo)
        self.woOpenButton.clicked.connect(self.openWO)
        self.woCloseButton.clicked.connect(self.closeWO)

        self.filterLineEdit.textChanged.connect(self.filterContent)

        self.woMgr = WorkContextTracer()

    def show(self):

        workContextViewLayer = self.woMgr.getWorkContextLayer()

        if workContextViewLayer is not None:
            self.cache = QgsVectorLayerCache(workContextViewLayer, 100)
            self.sortModel = QgsAttributeTableModel(self.cache)
            self.sortModel.loadLayer()
            
            self.model = QgsAttributeTableFilterModel(iface.mapCanvas(), self.sortModel, self)
            self.workOrdersView.setModel(self.model)
            self.workOrdersView.resizeColumnToContents(0)
    
            self.filterContent(self.filterLineEdit.text())
    
            super(WorkContextTracerDialog, self).show()
        else:
            self.close()
            iface.messageBar().pushMessage(self.tr(u"Work Context layer is missing"), QgsMessageBar.WARNING, 3)

    def selectedFeature(self):
        fid = self.workOrdersView.model().rowToId(self.workOrdersView.selectionModel().currentIndex())
        feature = QgsFeature()
        self.workOrdersView.model().layerCache().featureAtId(fid, feature)
        return feature

    def newWo(self):
        self.woMgr.createWorkContext()

    def openWO(self):
        if self.workOrdersView.selectionModel().currentIndex().isValid():
            self.woMgr.openWorkContext(self.selectedFeature())
            self.close()
        else:
            iface.messageBar().pushMessage(self.tr(u"Invalid selection"), QgsMessageBar.WARNING, 3)
                
    def closeWO(self):
        self.woMgr.closeWorkContext(None)

    def filterContent(self, text):
        
        if text is not None and len(text) > 0:
        
            self.model.setFilterMode(QgsAttributeTableFilterModel.ShowFilteredList)
            filterReq = QgsFeatureRequest()
            filterReq.setFilterExpression("\"{0}\" ILIKE '%{1}%'".format(wcc.WC_NAME, text))
            self.model.setFilteredFeatures([f.id() for f in  self.model.layerCache().getFeatures(filterReq)])
           
        else:
            
            self.model.setFilterMode(QgsAttributeTableFilterModel.ShowAll)
