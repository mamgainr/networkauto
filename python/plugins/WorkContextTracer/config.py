# -*- coding: utf-8 -*-

ID_ATTR = "id"
WC_NAME = "work_context_name"
WCI_CLASS = "item_class"
WCI_ID = "item_id"
WCI_OPERATION = "item_operation"

WORK_CONTEXT_SCOPE = "gni"
WORK_CONTEXT_KEY = "work_context"
WORK_CONTEXT_LAYERS = "work_context_layers"

WORK_CONTEXT_TABLE_NAME = "work_context"
WORK_CONTEXT_ITEM_TABLE_NAME = "work_context_item"