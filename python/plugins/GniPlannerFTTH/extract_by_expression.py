# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import  QgsExpression, QgsFeatureRequest

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterVector, ParameterString
from processing.core.outputs import OutputVector
from processing.tools import dataobjects
from processing.core.GeoAlgorithmExecutionException import GeoAlgorithmExecutionException

class ExtractByExpression(GeoAlgorithm):
    
    INPUT = 'INPUT'
    EXPRESSION = 'EXPRESSION'
    OUTPUT = 'OUTPUT'

    def defineCharacteristics(self):
        self.name = self.tr('Extract by expression')
        self.group = self.tr('UTIL')

        self.addParameter(ParameterVector(self.INPUT,
                                          self.tr('Input Layer'), [ParameterVector.VECTOR_TYPE_ANY]))
        self.addParameter(ParameterString(self.EXPRESSION,
                                          self.tr("Expression")))
        
        self.addOutput(OutputVector(self.OUTPUT, self.tr('Extracted')))


    def processAlgorithm(self, progress):
        
        filename = self.getParameterValue(self.INPUT)
        layer = dataobjects.getObjectFromUri(filename)

        output = self.getOutputFromName(self.OUTPUT)
        writer = output.getVectorWriter(layer.pendingFields(),
                                        layer.wkbType(), layer.crs())

        expression = self.getParameterValue(self.EXPRESSION)
        exp = QgsExpression(expression)
        if exp.hasParserError():
            raise GeoAlgorithmExecutionException(exp.parserErrorString())

        request = QgsFeatureRequest(exp)
        features = layer.getFeatures(request)
        
        progress.setInfo(self.tr("Loading selected features..."))
        progress.setPercentage(40)
        
        for f in features:
            writer.addFeature(f)
        
        progress.setPercentage(100)
        
        del writer