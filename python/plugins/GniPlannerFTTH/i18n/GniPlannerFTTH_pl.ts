<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="pl_PL" sourcelanguage="en">
<context>
    <name>AddLayer</name>
    <message>
        <location filename="add_layer.py" line="45"/>
        <source>Add layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_layer.py" line="47"/>
        <source>UTIL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_layer.py" line="50"/>
        <source>Input layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_layer.py" line="53"/>
        <source>Display name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_layer.py" line="69"/>
        <source>No work context available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_layer.py" line="72"/>
        <source>Plan - %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckWorkContext</name>
    <message>
        <location filename="check_work_context.py" line="34"/>
        <source>Check work context</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="check_work_context.py" line="36"/>
        <source>UTIL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="check_work_context.py" line="41"/>
        <source>No work context available.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DesignNetwork</name>
    <message>
        <location filename="design_network.py" line="55"/>
        <source>MAIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="53"/>
        <source>Design network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="58"/>
        <source>Potential nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="61"/>
        <source>Cable route</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="89"/>
        <source>No work context available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="152"/>
        <source>Creating pricing lines...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="163"/>
        <source>Filling paths details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="142"/>
        <source>Creating pricing points...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="175"/>
        <source>Missing layer {0}. Paths costs will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="205"/>
        <source>Missing OLT node.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="252"/>
        <source>Missing node {0} parent. Node will not be created </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="64"/>
        <source>Demand points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="67"/>
        <source>Demand id field name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="70"/>
        <source>Demand address field name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="design_network.py" line="166"/>
        <source>Filling demand details...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractByExpression</name>
    <message>
        <location filename="extract_by_expression.py" line="42"/>
        <source>Input Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_expression.py" line="44"/>
        <source>Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_expression.py" line="67"/>
        <source>Loading selected features...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_expression.py" line="39"/>
        <source>Extract by expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_expression.py" line="40"/>
        <source>UTIL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_expression.py" line="47"/>
        <source>Extracted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractByExtent</name>
    <message>
        <location filename="extract_by_extent.py" line="41"/>
        <source>Extract by extent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_extent.py" line="43"/>
        <source>UTIL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_extent.py" line="46"/>
        <source>Layer to select from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_extent.py" line="49"/>
        <source>Extent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_extent.py" line="52"/>
        <source>Extracted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extract_by_extent.py" line="71"/>
        <source>Loading extracted features...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GetExtent</name>
    <message>
        <location filename="get_extent.py" line="46"/>
        <source>layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="get_extent.py" line="55"/>
        <source>extent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="get_extent.py" line="49"/>
        <source>Destination CRS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="get_extent.py" line="52"/>
        <source>Use project CRS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="get_extent.py" line="42"/>
        <source>Get layer extent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="get_extent.py" line="44"/>
        <source>UTIL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GniPlannerFTTHProvider</name>
    <message>
        <location filename="gniplanner_provider.py" line="73"/>
        <source>Gni Planner FTTH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gniplanner_provider.py" line="57"/>
        <source>Intermediate layers crs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GniPlannerUtility</name>
    <message>
        <location filename="utility.py" line="62"/>
        <source>layer {0} not found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GniReprojectLayer</name>
    <message>
        <location filename="gni_reproject.py" line="43"/>
        <source>Input layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gni_reproject.py" line="46"/>
        <source>Reprojected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gni_reproject.py" line="39"/>
        <source>Reproject to intermediate crs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gni_reproject.py" line="41"/>
        <source>UTIL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HightLevelDesign</name>
    <message>
        <location filename="hight_level_design.py" line="62"/>
        <source>MAIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="60"/>
        <source>Hight level design</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="65"/>
        <source>Demand points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="68"/>
        <source>OLT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="71"/>
        <source>Route line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="74"/>
        <source>Aggregated demand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="80"/>
        <source>Path sharing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="77"/>
        <source>Splitter size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="235"/>
        <source>Missing attribute %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="264"/>
        <source>Unknown demand type. Supported values: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="134"/>
        <source>Bad or missing OLT feature.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="110"/>
        <source>No work context available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="159"/>
        <source>Searching places for final nodes...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="176"/>
        <source>Searching paths...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="139"/>
        <source>Searching for OLT vertex...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="127"/>
        <source>OLT not selected. First feature will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="206"/>
        <source>Adding branching nodes...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="188"/>
        <source>Creating routes...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="84"/>
        <source>Potential nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="86"/>
        <source>Cable route</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hight_level_design.py" line="117"/>
        <source>Data preparation...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlannerReport</name>
    <message>
        <location filename="planner_report.py" line="49"/>
        <source>MAIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="planner_report.py" line="55"/>
        <source>File path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="planner_report.py" line="86"/>
        <source>cost_distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="planner_report.py" line="52"/>
        <source>Open report file when finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="planner_report.py" line="68"/>
        <source>No work context available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="planner_report.py" line="47"/>
        <source>Generate report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="planner_report.py" line="83"/>
        <source>serving_area</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrepareDemandFields</name>
    <message>
        <location filename="prepare_demand_fields.py" line="49"/>
        <source>UTIL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="prepare_demand_fields.py" line="52"/>
        <source>Demand points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="prepare_demand_fields.py" line="61"/>
        <source>Prepared</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="prepare_demand_fields.py" line="47"/>
        <source>Prepare demand fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="prepare_demand_fields.py" line="55"/>
        <source>Demand field name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="prepare_demand_fields.py" line="58"/>
        <source>Demand type field name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
