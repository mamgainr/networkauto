# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import QgsCoordinateReferenceSystem, QgsCoordinateTransform, QgsFeature
from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterVector
from processing.core.outputs import OutputVector
from processing.tools import dataobjects, vector

from utility import getIntermediateLayersCrs 

class GniReprojectLayer(GeoAlgorithm):

    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'

    def defineCharacteristics(self):
        
        self.name = self.tr('Reproject to intermediate crs')
        
        self.group = self.tr('UTIL')

        self.addParameter(ParameterVector(self.INPUT,
                                          self.tr('Input layer'), [ParameterVector.VECTOR_TYPE_ANY]))

        self.addOutput(OutputVector(self.OUTPUT, self.tr('Reprojected')))

    def processAlgorithm(self, progress):
        layer = dataobjects.getObjectFromUri(self.getParameterValue(self.INPUT))
       
        crsId = getIntermediateLayersCrs()
        targetCrs = QgsCoordinateReferenceSystem()
        targetCrs.createFromUserInput(crsId)

        layerCrs = layer.crs()
        
        writer = self.getOutputFromName(self.OUTPUT).getVectorWriter(
            layer.pendingFields().toList(), layer.wkbType(), targetCrs)
        
        if targetCrs is not None and targetCrs != layerCrs:
            crsTransform = QgsCoordinateTransform(layerCrs, targetCrs)
        
        
            outFeat = QgsFeature()
            features = vector.features(layer)
            total = 100.0 / len(features)
            for current, f in enumerate(features):
                geom = f.geometry()
                geom.transform(crsTransform)
                outFeat.setGeometry(geom)
                outFeat.setAttributes(f.attributes())
                writer.addFeature(outFeat)
    
                progress.setPercentage(int(current * total))
    
            del writer

        else:
            self.setOutputValue(self.OUTPUT, self.getParameterValue(self.INPUT))
