# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""


from PyQt4.QtCore import QVariant

from qgis.core import QgsFeature, QgsField

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterVector, ParameterTableField
from processing.core.outputs import OutputVector
from processing.tools import dataobjects, vector

from utility import resolveFieldIndex
import config as cf

class PrepareDemandFields(GeoAlgorithm):

    DEMAND_LAYER = 'DEMAND_LAYER'
    DEMAND_FIELD = 'DEMAND_FIELD'
    DEMAND_TYPE_FIELD = 'DEMAND_TYPE_FIELD'
    
    OUTPUT_LAYER = 'OUTPUT_LAYER'
    
    def defineCharacteristics(self):
        
        self.name = self.tr('Prepare demand fields')

        self.group = self.tr('UTIL')


        self.addParameter(ParameterVector(self.DEMAND_LAYER,
            self.tr('Demand points'), [ParameterVector.VECTOR_TYPE_ANY]))
        
        self.addParameter(ParameterTableField(self.DEMAND_FIELD,
            self.tr('Demand field name'), self.DEMAND_LAYER, -1, False))

        self.addParameter(ParameterTableField(self.DEMAND_TYPE_FIELD,
            self.tr('Demand type field name'), self.DEMAND_LAYER, -1, True))

        self.addOutput(OutputVector(self.OUTPUT_LAYER, self.tr('Prepared')))

    def processAlgorithm(self, progress):
        
        filename = self.getParameterValue(self.DEMAND_LAYER)
        layer = dataobjects.getObjectFromUri(filename)

        demandField = self.getParameterValue(self.DEMAND_FIELD)
        demandTypeField = self.getParameterValue(self.DEMAND_TYPE_FIELD)


        fields = layer.fields()
        if demandField != cf.DEMAND_FIELD_NAME:
            fields.append(QgsField(cf.DEMAND_FIELD_NAME, QVariant.Int, '', 10))
        fields.append(QgsField(cf.DEMAND_TYPE_FIELD_NAME, QVariant.String, '', 50))

        output = self.getOutputFromName(self.OUTPUT_LAYER)
        writer = output.getVectorWriter(fields, layer.wkbType(), layer.crs())

        demandIndex = resolveFieldIndex(layer, demandField)

        outFeat = QgsFeature()
        features = vector.features(layer)
        total = 100.0 / len(features)
        for current, feat in enumerate(features):
            geom = feat.geometry()
            outFeat.setGeometry(geom)
            demandValue = feat.attributes()[demandIndex]
            
            attributes = feat.attributes()
            
            if  demandField != cf.DEMAND_FIELD_NAME:
                attributes.append(feat.attributes()[demandIndex])
                
            if demandTypeField is not None:
                attributes.append(feat.attribute(demandTypeField))
            else:
                attributes.append(cf.BASIC_DEMAND_TYPE if demandValue < 2 else cf.AGGREGATED_DEMAND_TYPE)
            
            outFeat.setAttributes(attributes)
            
            writer.addFeature(outFeat)
            progress.setPercentage(int(current * total))
            
        del writer
    
