# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication

from processing.core.Processing import Processing
from processing.core.ProcessingConfig import ProcessingConfig
from processing.core.alglist import algList
from processing.modeler.ModelerUtils import ModelerUtils
#from processing.gui.menus import addAlgorithmEntry, removeAlgorithmEntry

from gniplanner_provider import GniPlannerFTTHProvider

class GniPlannerFTTHPlugin:

    MODELS_FOLDER_KEY = u'Processing/Configuration/MODELS_FOLDER'
    PATH_SEPARATOR = ";"

    def __init__(self):
        
        self.plugin_dir = os.path.dirname(__file__)

        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'GniPlannerFTTH_{}.qm'.format(locale))
 
        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)
        
        self.provider = GniPlannerFTTHProvider()
        
        self.modelsPath = os.path.join(self.plugin_dir, 'models')

    def initGui(self):  #TODO: przy zbyt szybkim przeladowaniu wylacza inne providery
        Processing.addProvider(self.provider)
        
        s = QSettings()
        
        currentPaths = ModelerUtils.modelsFolders()
        if currentPaths is not None:
            s.setValue(self.MODELS_FOLDER_KEY, self.PATH_SEPARATOR.join(currentPaths + [self.modelsPath]))
        else:
            s.setValue(self.MODELS_FOLDER_KEY, self.modelsPath)
        
        s.sync()
        ProcessingConfig.readSettings()
        algList.reloadProvider('model')

    def unload(self):
        Processing.removeProvider(self.provider)
        
        s = QSettings()
        
        currentPaths = s.value(self.MODELS_FOLDER_KEY)
        paths = currentPaths.split(self.PATH_SEPARATOR)

        filtered = [path for path in paths if path != self.modelsPath]
        s.setValue(self.MODELS_FOLDER_KEY, self.PATH_SEPARATOR.join(filtered))
        
        s.sync()
        ProcessingConfig.readSettings()
        algList.reloadProvider('model')
