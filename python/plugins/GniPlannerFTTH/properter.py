# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.networkanalysis import QgsArcProperter

from utility import resolveFieldIndex

class CopyProperter(QgsArcProperter):
    
    def __init__(self, layer, propertyName):
        super(CopyProperter, self).__init__()
        self.propertyName = propertyName
        self.index = resolveFieldIndex(layer, propertyName)
        
    def requiredAttributes(self):
        return [self.index]
        
    def property(self, distance, f):
        return f.attribute(self.propertyName)

class FeatureIdProperter(QgsArcProperter):
    
    def __init__(self):
        super(FeatureIdProperter, self).__init__()

    def requiredAttributes(self):
        return []
        
    def property(self, distance, f):
        return f.id()

class CostFactorProperter(QgsArcProperter):
    
    def __init__(self, layer, propertyName):
        super(CostFactorProperter, self).__init__()
        self.propertyName = propertyName
        self.index = resolveFieldIndex(layer, propertyName)
        
    def requiredAttributes(self):
        return [self.index]
        
    def property(self, distance, f):
        return f.attribute(self.propertyName) * distance
