# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface): 
    """Load GniPlannerFTTH class from file GniIntegration.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """

    from gniplanner import GniPlannerFTTHPlugin
    return GniPlannerFTTHPlugin()
