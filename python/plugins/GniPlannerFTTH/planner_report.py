# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os, sys, subprocess

from openpyxl import Workbook

from qgis.core import QgsFeatureRequest, NULL  #@UnresolvedImport

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterBoolean
from processing.core.outputs import OutputFile
from processing.core.GeoAlgorithmExecutionException import GeoAlgorithmExecutionException

from utility import resolveLayerByTableName

from WorkContextTracer.workcontexttracer import WorkContextTracer

class PlannerReport(GeoAlgorithm):

    OPEN_FILE = 'OPEN_FILE'

    OUTPUT_FILE = 'OUTPUT_FILE'

    def defineCharacteristics(self):

        self.name = self.tr('Generate report')

        self.group = self.tr('MAIN')

    
        self.addParameter(ParameterBoolean(self.OPEN_FILE,
            self.tr("Open report file when finished"), True, False))
    
        self.addOutput(OutputFile(self.OUTPUT_FILE,
            self.tr('File path'), 'xlsx'))
        
    
    def processAlgorithm(self, progress):
        
        openFile = self.getParameterValue(self.OPEN_FILE)
        
        filePath = self.getOutputValue(self.OUTPUT_FILE)
        
        if WorkContextTracer().isActiveWorkContext():
            workContextName = WorkContextTracer().activeWorkContext()
        else:
            raise GeoAlgorithmExecutionException(self.tr('No work context available.')) 
        
        progress.setInfo(str(filePath))
        
                    
        workContextLabel = "".join(x for x in workContextName if x.isalnum())
        
        wb = Workbook()
        
        wb.worksheets[0].title = workContextLabel

        self.writeReportPart(wb.worksheets[0], workContextName, "vw_ftth_planner_report")

        progress.setPercentage(40)

        wb.create_sheet(self.tr("serving_area"))
        self.writeReportPart(wb.worksheets[1], workContextName, "vw_ftth_planner_report_location")

        wb.create_sheet(self.tr("cost_distribution"))
        self.writeReportPart(wb.worksheets[2], workContextName, "vw_ftth_planner_report_distribution")

        progress.setPercentage(80)
        
        wb.save(filePath)
        
        if openFile:
            if sys.platform == "win32":
                os.system("start " + filePath)
            else:
                subprocess.call(["xdg-open", filePath])
            
        progress.setPercentage(100)
         
    def writeReportPart(self, worksheet, workContextName, viewName):
        
        reportLayer = resolveLayerByTableName(viewName)
        fields = reportLayer.fields()
        
        req = QgsFeatureRequest()
        req.setFilterExpression("{0} = '{1}'".format(fields[1].name(), workContextName))
        
        for i in range(1, len(fields)):
            worksheet.cell(row = 1 , column = i , value = fields[i].displayName())
        
        for i, feature in enumerate(reportLayer.getFeatures(req), 2):

            row = feature.attributes()

            for j in range(1, len(fields)):
            
                cellValue = u''
                try:
                    cellValue = unicode(row[j]) if row[j] != NULL else u''
                    cellValue = float(row[j])
                except Exception:
                    pass
                
                worksheet.cell(row = i, column = j , value = cellValue)
