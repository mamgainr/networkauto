# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from processing.core.AlgorithmProvider import AlgorithmProvider
from processing.core.ProcessingConfig import ProcessingConfig, Setting

from hight_level_design import HightLevelDesign
from design_network import DesignNetwork
from get_extent import GetExtent
from extract_by_extent import ExtractByExtent
from prepare_demand_fields import PrepareDemandFields
from planner_report import PlannerReport
from gni_reproject import GniReprojectLayer
from extract_by_expression import ExtractByExpression
from add_layer import AddLayer
from check_work_context import CheckWorkContext

class GniPlannerFTTHProvider(AlgorithmProvider):

    SETTING_GROUP = 'Gni Planner FTTH'
    INTERMEDIATE_LAYER_CRS = 'INTERMEDIATE_LAYER_CRS'

    def __init__(self):
        AlgorithmProvider.__init__(self)

        self.activate = True

        self.alglist = [HightLevelDesign(), DesignNetwork(), GetExtent(), \
                        ExtractByExtent(), PrepareDemandFields(), PlannerReport(), \
                        GniReprojectLayer(), ExtractByExpression(), AddLayer(), CheckWorkContext()]
        for alg in self.alglist:
            alg.provider = self

    def initializeSettings(self):
        """In this method we add settings needed to configure our provider."""
        AlgorithmProvider.initializeSettings(self)
        ProcessingConfig.addSetting(Setting(self.SETTING_GROUP, self.INTERMEDIATE_LAYER_CRS, \
                self.tr("Intermediate layers crs"), "EPSG:3857", False, 0))

    def unload(self):
        """Setting should be removed here, so they do not appear anymore
        when the plugin is unloaded.
        """
        AlgorithmProvider.unload(self)
        ProcessingConfig.removeSetting(self.INTERMEDIATE_LAYER_CRS)

    def getName(self):
        """This is the name that will appear on the toolbox group."""
        return 'Gni Planner FTTH'

    def getDescription(self):
        """This is the provired full name."""
        return self.tr('Gni Planner FTTH')

    def getIcon(self):
        """We return the default icon."""
        return AlgorithmProvider.getIcon(self)

    def _loadAlgorithms(self):

        self.algs = self.alglist

    def supportsNonFileBasedOutput(self):
        return True
