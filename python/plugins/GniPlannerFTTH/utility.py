# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import uuid

from PyQt4.QtCore import QCoreApplication

from qgis.core import QgsField, QgsMapLayerRegistry, QgsDataSourceURI, QgsLogger

from processing.core.ProcessingConfig import ProcessingConfig
from processing.core.GeoAlgorithmExecutionException import GeoAlgorithmExecutionException

def resolveFieldIndex(layer, attr):

    index = layer.fieldNameIndex(unicode(attr))
    if index == -1:
        index = layer.fieldNameIndex(unicode(attr[:10]))
    return index


def addFieldToLayer(layer, fieldType, fieldName):
    
    fieldIndex = resolveFieldIndex(layer, fieldName)
    if fieldIndex <> -1:  
        return fieldIndex
    layer.dataProvider().addAttributes([QgsField(fieldName, fieldType)])
    layer.updateFields()
    return resolveFieldIndex(layer, fieldName)


def getLayerByTableName(table):
    layers = [lyr for lyr in QgsMapLayerRegistry.instance().mapLayers().values()\
               if QgsDataSourceURI(lyr.dataProvider().dataSourceUri()).table() == table]
    if len(layers) == 1:
        return layers[0]
    else:
        QgsLogger.debug("getLayerByTableName failed: " + table)
        return None

def resolveLayerByTableName(table):
    layer = getLayerByTableName(table)
    if layer is None:
        raise GeoAlgorithmExecutionException(QCoreApplication.translate('GniPlannerUtility', u'layer {0} not found.').format(table))
    else:
        return layer
    
def getUUID():
    return uuid.uuid4()

def getIntermediateLayersCrs():
    from gniplanner_provider import GniPlannerFTTHProvider
    return ProcessingConfig.getSetting(GniPlannerFTTHProvider.INTERMEDIATE_LAYER_CRS)
    
