# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import QgsFeatureRequest, QgsRectangle

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterVector, ParameterExtent
from processing.core.outputs import OutputVector
from processing.tools import dataobjects


class ExtractByExtent(GeoAlgorithm):

    INPUT_LAYER = 'INPUT_LAYER'
    EXTENT = 'EXTENT'

    OUTPUT_LAYER = 'OUTPUT_LAYER'

    def defineCharacteristics(self):
        
        self.name = self.tr('Extract by extent')

        self.group = self.tr('UTIL')


        self.addParameter(ParameterVector(self.INPUT_LAYER,
            self.tr('Layer to select from'), [ParameterVector.VECTOR_TYPE_ANY]))

        self.addParameter(ParameterExtent(self.EXTENT, self.tr('Extent')))

        
        self.addOutput(OutputVector(self.OUTPUT_LAYER, self.tr('Extracted')))

    def processAlgorithm(self, progress):
        
        filename = self.getParameterValue(self.INPUT_LAYER)
        layer = dataobjects.getObjectFromUri(filename)
        
        extent = self.getParameterValue(self.EXTENT)

        output = self.getOutputFromName(self.OUTPUT_LAYER)
        writer = output.getVectorWriter(layer.pendingFields(),
                                        layer.wkbType(), layer.crs())

        extent = [float(p) for p in extent.split(',')]
        rectangle = QgsRectangle(extent[0], extent[2], extent[1], extent[3])
        
        request = QgsFeatureRequest(rectangle)
        features = layer.getFeatures(request)
        
        progress.setInfo(self.tr("Loading extracted features..."))
        progress.setPercentage(40)
        
        for f in features:
            writer.addFeature(f)
        
        progress.setPercentage(100)
        
        del writer
