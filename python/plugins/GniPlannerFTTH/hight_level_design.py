# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import math

from PyQt4.QtCore import QVariant, QPyNullVariant  #@UnresolvedImport

from qgis.core import QGis, QgsFeature, QgsFeatureRequest, QgsVectorLayer, QgsSpatialIndex, \
                     QgsFields, QgsField , QgsGeometry, QgsCoordinateReferenceSystem

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterVector, ParameterNumber
from processing.core.outputs import OutputVector
from processing.tools import dataobjects, vector
from processing.core.GeoAlgorithmExecutionException import GeoAlgorithmExecutionException

from qgis.networkanalysis import QgsLineVectorLayerDirector, QgsGraphAnalyzer, QgsGraphBuilder

from properter import FeatureIdProperter, CostFactorProperter
from utility import addFieldToLayer, resolveFieldIndex, getUUID, getIntermediateLayersCrs

import config as cf

from WorkContextTracer.workcontexttracer import WorkContextTracer

class HightLevelDesign(GeoAlgorithm):

    DEMAND_LAYER = 'DEMAND_LAYER'
    OLT_LAYER = 'OLT_LAYER'
    ROUTE_LINE_LAYER = 'ROUTE_LINE_LAYER'
    AGGREGARED_DEMAND = 'AGGREGARED_DEMAND'
    SPLITTER = 'SPLITTER'
    PATH_SHARING = 'PATH_SHARING'
    
    OUT_NODE = 'OUT_NODE'
    OUT_CABLE = 'OUT_CABLE'

    def defineCharacteristics(self):

        self.name = self.tr('Hight level design')

        self.group = self.tr('MAIN')

    
        self.addParameter(ParameterVector(self.DEMAND_LAYER,
            self.tr('Demand points'), [ParameterVector.VECTOR_TYPE_POINT], False))
        
        self.addParameter(ParameterVector(self.OLT_LAYER,
            self.tr('OLT'), [ParameterVector.VECTOR_TYPE_POINT], False))

        self.addParameter(ParameterVector(self.ROUTE_LINE_LAYER,
            self.tr('Route line'), [ParameterVector.VECTOR_TYPE_LINE], False))
        
        self.addParameter(ParameterVector(self.AGGREGARED_DEMAND,
            self.tr('Aggregated demand'), [ParameterVector.VECTOR_TYPE_POINT], False))

        self.addParameter(ParameterNumber(self.SPLITTER,
            self.tr('Splitter size'), 2, None, 32, False))

        self.addParameter(ParameterNumber(self.PATH_SHARING,
            self.tr('Path sharing'), 0.0, 1.0, 0.5, False))

        
        self.addOutput(OutputVector(self.OUT_NODE, self.tr('Potential nodes')))
        
        self.addOutput(OutputVector(self.OUT_CABLE, self.tr('Cable route')))
    

    def processAlgorithm(self, progress):
        
        demandLayerFile = self.getParameterValue(self.DEMAND_LAYER)
        oltLayerFile = self.getParameterValue(self.OLT_LAYER)
        routeLineLayerFile = self.getParameterValue(self.ROUTE_LINE_LAYER)
        aggregatedDemandLayerFile = self.getParameterValue(self.AGGREGARED_DEMAND)
        splitterSize = int(self.getParameterValue(self.SPLITTER))
        self.pathSharing = self.getParameterValue(self.PATH_SHARING)
        
        
        demandLayer = dataobjects.getObjectFromUri(demandLayerFile)
        oltLayer = dataobjects.getObjectFromUri(oltLayerFile)
        routeLineLayer = dataobjects.getObjectFromUri(routeLineLayerFile)
        aggregatedDemandLayer = dataobjects.getObjectFromUri(aggregatedDemandLayerFile)
        
        outputNode = self.getOutputFromName(self.OUT_NODE)
        outputCable = self.getOutputFromName(self.OUT_CABLE)

        if WorkContextTracer().isActiveWorkContext():
            self.planName = WorkContextTracer().activeWorkContext()
        else:
            raise GeoAlgorithmExecutionException(self.tr('No work context available.')) 
        
        self.crsAuthid = getIntermediateLayersCrs()
        
        self.nodes = []
        self.routeIds = set()
        
        progress.setInfo(self.tr('Data preparation...'))
        
        groupQuantity = self.aggregateDemand(demandLayer, aggregatedDemandLayer, splitterSize)

        graphLayer = self.prepareGraphLayer(routeLineLayer)    
        self.graph = self.createGraph(graphLayer)
                  
        self.prepareOutput(outputNode, outputCable)
                
        if oltLayer.selectedFeatureCount() == 0:
            progress.setInfo(self.tr('OLT not selected. First feature will be used.'))
            oltFeature = QgsFeature()
            oltLayer.getFeatures().nextFeature(oltFeature)
        else:
            oltFeature = oltLayer.selectedFeatures()[0]
        
        if oltFeature.geometry() is None:
            raise GeoAlgorithmExecutionException(self.tr(u'Bad or missing OLT feature.'))
        

        (pointSpatialIndexLayer, spIndex) = self.createSpatialIndexFromGraph(progress)

        progress.setInfo(self.tr('Searching for OLT vertex...'))
        oltFeatureVertex = self.graph.findVertex(oltFeature.geometry().asPoint())
        if oltFeatureVertex == -1:
            nearestIds = spIndex.nearestNeighbor(oltFeature.geometry().asPoint(), 1)
            nearestFeature = QgsFeature()
            pointSpatialIndexLayer.getFeatures(QgsFeatureRequest(nearestIds[0])).nextFeature(nearestFeature)
            oltFeatureVertex = self.graph.findVertex(nearestFeature.geometry().asPoint())
            oltFeature.setGeometry(nearestFeature.geometry())
        
        oltSplice = QgsFeature()
        oltSplice.setFields(self.nodesFields)
        oltSplice.setGeometry(oltFeature.geometry())
        oltSplice.setAttribute(cf.NODE_ID_FIELD_NAME, 1)
        oltSplice.setAttribute(cf.NODE_TYPE_FIELD_NAME, cf.NODE_TYPE_OLT)
        self.nodeWriter.addFeature(oltSplice)
        self.nodes.append(oltSplice)

        endingFeatures = {}
        endingFeaturesDistance = {}
         
        progress.setInfo(self.tr('Searching places for final nodes...'))
        for pointPE in aggregatedDemandLayer.getFeatures():
            point = pointPE.geometry().asPoint()

            nearestIds = spIndex.nearestNeighbor(point, 1)
            fid = nearestIds[0]

            nearestFeature = QgsFeature()
            pointSpatialIndexLayer.getFeatures(QgsFeatureRequest(fid)).nextFeature(nearestFeature)
            geomPoint = nearestFeature.geometry().asPoint()
            feature = self.prepareEndingNodes(geomPoint, pointPE, groupQuantity)
            vertexZlacze = self.graph.findVertex(geomPoint)
            endingFeatures[vertexZlacze] = feature
            endingFeaturesDistance[vertexZlacze] = self.euclideanDistance(geomPoint, oltFeature.geometry().asPoint())
        
        endingFeaturesDistanceSorted = sorted(endingFeaturesDistance.items(), key = lambda value: value[1])      
        
        progress.setInfo(self.tr('Searching paths...'))
        total = 100.0 / len(endingFeaturesDistanceSorted)
        for current, (key, _) in enumerate(endingFeaturesDistanceSorted):
            if current > 0:
                self.graph = self.createGraph(graphLayer)
        
            self.makeConnection(graphLayer, key, oltFeatureVertex)
            progress.setPercentage(int(current * total))
        
        progress.setPercentage(100)
        
        
        progress.setInfo(self.tr('Creating routes...'))
        self.amountOfPoints = {}

        features = vector.features(graphLayer)
        total = 100.0 / len(features)
        nextRouteId = 1
        for current, f in enumerate(features):
            if f.id() in self.routeIds:
                featureLine = QgsFeature()
                featureLine.setFields(self.cablesFields)
                featureLine.setGeometry(f.geometry())
                featureLine.setAttribute(cf.CABLE_ID_FIELD_NAME, nextRouteId)
                self.cableWriter.addFeature(featureLine)
                self.countNetworkPoints(featureLine.geometry().asPolyline())
                nextRouteId = nextRouteId + 1
            progress.setPercentage(int(current * total))

        
        progress.setInfo(self.tr('Adding branching nodes...'))
        nextNodeId = len(self.nodes) + 1

        for point in self.amountOfPoints.keys():
            if self.amountOfPoints[point] > 2:
                feat = QgsFeature()
                feat.setGeometry(QgsGeometry.fromPoint(point))
                feat.setFields(self.nodesFields)
                feat.setAttribute(cf.NODE_ID_FIELD_NAME, nextNodeId)
                feat.setAttribute(cf.NODE_TYPE_FIELD_NAME, cf.NODE_TYPE_BRANCHING)
                if not self.existSplice(feat.geometry(), self.nodes):
                    self.nodeWriter.addFeature(feat)
                    self.nodes.append(feat)
                    nextNodeId = nextNodeId + 1 
       
        
        del self.nodeWriter
        del self.cableWriter

    def aggregateDemand(self, demandLayer, aggregatedDemandLayer, splitterSize):

        demandFieldIndex = resolveFieldIndex(demandLayer, cf.DEMAND_FIELD_NAME) 
        if demandFieldIndex == -1:
            raise GeoAlgorithmExecutionException(self.tr(u'Missing attribute %s') % cf.DEMAND_FIELD_NAME)
        demandGroupIndex = resolveFieldIndex(demandLayer, cf.DEMAND_GROUP_FIELD_NAME)
        if demandGroupIndex == -1:
            raise GeoAlgorithmExecutionException(self.tr(u'Missing attribute %s') % cf.DEMAND_GROUP_FIELD_NAME)
        demandTypeFieldIndex = resolveFieldIndex(demandLayer, cf.DEMAND_TYPE_FIELD_NAME)
        if demandTypeFieldIndex == -1:
            raise GeoAlgorithmExecutionException(self.tr(u'Missing attribute %s') % cf.DEMAND_TYPE_FIELD_NAME)

        agreG = {}
        notAgreg = {}
        
        groupQuantity = {}
        
        groups = demandLayer.uniqueValues(demandGroupIndex)
        groups = [int(x) for x in groups if type(x) <> QPyNullVariant]
        
        for group in groups:
                      
            groupQuantity[group] = 0
            agreG[group] = 0
            notAgreg[group] = 0
            
            exp = u'{0} = {1}'.format(cf.DEMAND_GROUP_FIELD_NAME, group)
            featuresFromGroup = demandLayer.getFeatures(QgsFeatureRequest().setFilterExpression(exp))
            for featureFromGroup in featuresFromGroup:
                groupQuantity[group] = groupQuantity[group] + 1
                
                demandType = featureFromGroup.attributes()[demandTypeFieldIndex]
                popytWartosc = featureFromGroup.attributes()[demandFieldIndex]  #TODO: !
                
                if demandType == cf.AGGREGATED_DEMAND_TYPE or type(demandType) == QPyNullVariant:
                    agreG[group] = agreG[group] + int(popytWartosc)
                elif demandType == cf.BASIC_DEMAND_TYPE:
                    notAgreg[group] = notAgreg[group] + int(popytWartosc)
                else:
                    raise GeoAlgorithmExecutionException(self.tr(u'Unknown demand type. Supported values: ') + ','.join(cf.DEMAND_TYPES))
        
        popytPE2idx = addFieldToLayer(aggregatedDemandLayer, QVariant.Int, cf.DEMAND_FIELD_NAME)
        uidPE2idx = resolveFieldIndex(aggregatedDemandLayer, cf.MEAN_COORDINATE_FIELD_NAME)
        
        aggregatedDemandLayer.startEditing()
        for punktPe2 in aggregatedDemandLayer.getFeatures():
            if type(punktPe2.attributes()[uidPE2idx]) == QPyNullVariant or punktPe2.attributes()[uidPE2idx] is None or str(punktPe2.attributes()[uidPE2idx]) == 'NULL' :
                aggregatedDemandLayer.deleteFeature(punktPe2.id())
                continue
            grupa = int(punktPe2.attributes()[uidPE2idx])
            popytAgr = agreG[grupa]
            popytNonAgr = notAgreg[grupa]
            roundValue = round((popytNonAgr / splitterSize) + 0.51)
            if popytNonAgr > 0 and roundValue >= 0 and roundValue < 1.0:
                roundValue = 1
            wartosc = round(int(popytAgr)) + roundValue
            aggregatedDemandLayer.changeAttributeValue(punktPe2.id(), popytPE2idx, round(int(wartosc)))
        aggregatedDemandLayer.commitChanges()
        
        return groupQuantity
    
    def prepareOutput(self, outputNode, outputCable):
        
        crs = QgsCoordinateReferenceSystem()
        crs.createFromUserInput(self.crsAuthid)
        
        self.nodesFields = QgsFields()
        self.nodesFields.append(QgsField(cf.NODE_ID_FIELD_NAME, QVariant.Int))
        self.nodesFields.append(QgsField(cf.NODE_DEMAND_FIELD_NAME, QVariant.Int))
        self.nodesFields.append(QgsField(cf.NODE_TYPE_FIELD_NAME, QVariant.String))
        self.nodesFields.append(QgsField(cf.NODE_GROUP_FIELD_NAME, QVariant.String))
        self.nodesFields.append(QgsField(cf.NODE_DEM_PNT_COUNT_FIELD_NAME, QVariant.Int))
        self.nodesFields.append(QgsField(cf.NODE_PARENT_FIELD_NAME, QVariant.Int))
        
        self.nodeWriter = outputNode.getVectorWriter(self.nodesFields, QGis.WKBPoint, crs)
        
        self.cablesFields = QgsFields()
        self.cablesFields.append(QgsField(cf.CABLE_ID_FIELD_NAME, QVariant.Int))

        self.cableWriter = outputCable.getVectorWriter(self.cablesFields, QGis.WKBLineString, crs)
        
    
    def prepareGraphLayer(self, layer):

        graphLayer = QgsVectorLayer("LineString?crs=%s&memoryid={%s}" % (self.crsAuthid, getUUID()), 'graphLayer', "memory")

        graphLayer.dataProvider().addAttributes(\
            [QgsField(cf.GRAPH_COST_FACTOR_FIELD_NAME, QVariant.Double)])
        graphLayer.updateFields()

        graphFields = graphLayer.fields()

        graphLayer.startEditing()
        for feature in layer.getFeatures():
                        
            graphFeature = QgsFeature()
            graphFeature.setFields(graphFields)
            graphFeature.setAttributes([1.0])
            graphFeature.setGeometry(feature.geometry())

            graphLayer.addFeature(graphFeature)
        graphLayer.commitChanges()
        
        return graphLayer
    
    def createSpatialIndexFromGraph(self, progress):
        
        spatialIndexLayer = QgsVectorLayer("Point?crs=%s&memoryid={%s}" % (self.crsAuthid, getUUID()), 'spatialIndexLayer', "memory")
        spatialIndexLayer.startEditing()
        
        total = 100.0 / self.graph.vertexCount()
        for current, vertexIndex in enumerate(range(self.graph.vertexCount())):
            point = self.graph.vertex(vertexIndex).point()
        
            fet = QgsFeature()
            fet.setGeometry(QgsGeometry.fromPoint(point))

            spatialIndexLayer.addFeature(fet, False)
            
            progress.setPercentage(int(current * total))
            
        spatialIndexLayer.commitChanges()
        
        return (spatialIndexLayer, QgsSpatialIndex(spatialIndexLayer.getFeatures()))
    

    def createGraph(self, layer):
        director = QgsLineVectorLayerDirector(layer, -1, '', '', '', 3)
        
        idProperter = FeatureIdProperter()
        costProperter = CostFactorProperter(layer, cf.GRAPH_COST_FACTOR_FIELD_NAME)
        director.addProperter(idProperter)
        director.addProperter(costProperter)
        
        crs = layer.crs()
        builder = QgsGraphBuilder(crs)
        director.makeGraph(builder, [])
        return builder.graph()
     
    def existSplice(self, geom, existingNodes):
        for feature in existingNodes: 
            featureGeom = feature.geometry()
            if geom is not None and featureGeom.asPoint() == geom.asPoint():
                return True
        return False
            
    def euclideanDistance(self, point1, point2):
        return math.sqrt((point2.x() - point1.x()) ** 2 + (point2.y() - point1.y()) ** 2)
             
    def prepareEndingNodes(self, point, fromFeature, groupQuantity):
        
        fet = QgsFeature()
        fet.setGeometry(QgsGeometry.fromPoint(point))

        fet.setFields(self.nodesFields)
        
        demandValue = int(fromFeature.attribute(cf.DEMAND_FIELD_NAME))
        groupValue = fromFeature.attribute(cf.MEAN_COORDINATE_FIELD_NAME)
        
        fet.setAttribute(cf.NODE_ID_FIELD_NAME, int(len(self.nodes) + 1))
        fet.setAttribute(cf.NODE_DEMAND_FIELD_NAME, demandValue)
        fet.setAttribute(cf.NODE_TYPE_FIELD_NAME, cf.NODE_TYPE_FINAL)
        fet.setAttribute(cf.NODE_GROUP_FIELD_NAME, groupValue)
        fet.setAttribute(cf.NODE_DEM_PNT_COUNT_FIELD_NAME, groupQuantity[int(groupValue)])

        self.nodeWriter.addFeature(fet)
        self.nodes.append(fet)
        return fet

    def makeConnection(self, graphLayer, fromId, toId):

        (tree, _) = QgsGraphAnalyzer.dijkstra(self.graph, fromId, 1)

        if tree[toId] != -1:
            curPos = toId 
            graphLayer.startEditing()
            costFactorIndex = graphLayer.fieldNameIndex(cf.GRAPH_COST_FACTOR_FIELD_NAME)
            while (curPos != fromId):

                visitedArc = self.graph.arc(tree[ curPos ])
                fid = visitedArc.property(0)

                graphLayer.changeAttributeValue(fid, costFactorIndex, self.pathSharing)
                
                self.routeIds.add(fid)
                
                curPos = visitedArc.outVertex()
            graphLayer.commitChanges()
            
    def countNetworkPoints(self, line):

        firstPoint = line[0]
        lastPoint = line[-1]
        if self.amountOfPoints.has_key(firstPoint):
            self.amountOfPoints[firstPoint] = self.amountOfPoints[firstPoint] + 1
        else:
            self.amountOfPoints[firstPoint] = 1
        if self.amountOfPoints.has_key(lastPoint):
            self.amountOfPoints[lastPoint] = self.amountOfPoints[lastPoint] + 1
        else:
            self.amountOfPoints[lastPoint] = 1
            
