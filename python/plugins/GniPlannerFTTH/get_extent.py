# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from qgis.core import QgsCoordinateTransform, QgsCoordinateReferenceSystem
from qgis.utils import iface

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterVector, ParameterCrs, ParameterBoolean
from processing.core.outputs import OutputExtent
from processing.tools import dataobjects, vector


class GetExtent(GeoAlgorithm):

    INPUT_LAYER = 'INPUT_LAYER'
    CRS = 'CRS'
    PROJECT_CRS = 'PROJECT_CRS'

    OUTPUT_EXTENT = 'OUTPUT_EXTENT'

    def defineCharacteristics(self):

        self.name = self.tr('Get layer extent')
        
        self.group = self.tr('UTIL')
     
        self.addParameter(ParameterVector(self.INPUT_LAYER,
            self.tr('layer'), [ParameterVector.VECTOR_TYPE_ANY], False))
        
        self.addParameter(ParameterCrs(self.CRS,
            self.tr('Destination CRS'), None, True))
        
        self.addParameter(ParameterBoolean(self.PROJECT_CRS,
            self.tr('Use project CRS'), False))

        self.addOutput(OutputExtent(self.OUTPUT_EXTENT,
            self.tr('extent')))
        

    def processAlgorithm(self, progress):
        
        inputFile = self.getParameterValue(self.INPUT_LAYER)
        vectorLayer = dataobjects.getObjectFromUri(inputFile)
        
        destinationCrs = QgsCoordinateReferenceSystem(self.getParameterValue(self.CRS))

        useProjectCRS = self.getParameterValue(self.PROJECT_CRS)
        
        if useProjectCRS:
            destinationCrs = iface.mapCanvas().mapSettings().destinationCrs()

        layerCrs = vectorLayer.crs()
        
        crsTransform = None

        if destinationCrs is not None and destinationCrs != layerCrs:
            crsTransform = QgsCoordinateTransform(layerCrs, destinationCrs)
        
        features = vector.features(vectorLayer)
        
        extent = None
        for feature in features:
            geom = feature.geometry()
            if geom is not None:
                
                if crsTransform:
                    geom.transform(crsTransform)
                
                try:
                    extent.combineExtentWith(geom.boundingBox())
                except AttributeError:
                    extent = geom.boundingBox()
        
        output = [extent.xMinimum(), extent.xMaximum(), extent.yMinimum(), extent.yMaximum()]
        
        progress.setInfo(','.join([str(v) for v in output]))

        self.setOutputValue(self.OUTPUT_EXTENT, output)
