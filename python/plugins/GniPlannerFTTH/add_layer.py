# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import QgsMapLayerRegistry, QgsProject

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterVector, ParameterString
from processing.tools import dataobjects
from processing.core.GeoAlgorithmExecutionException import GeoAlgorithmExecutionException


from WorkContextTracer.workcontexttracer import WorkContextTracer


class AddLayer(GeoAlgorithm):

    INPUT_LAYER = 'INPUT_LAYER'
    LAYER_NAME = 'LAYER_NAME'


    MASTER_GRP_NAME = u'GNI Planner'
    
    def defineCharacteristics(self):

        self.name = self.tr('Add layer')

        self.group = self.tr('UTIL')

    
        self.addParameter(ParameterVector(self.INPUT_LAYER,
            self.tr('Input layer'), [ParameterVector.VECTOR_TYPE_ANY], False))

        self.addParameter(ParameterString(self.LAYER_NAME,
            self.tr('Display name')))


    def processAlgorithm(self, progress):
        
        
        inputLayer = self.getParameterValue(self.INPUT_LAYER)
        layerName = self.getParameterValue(self.LAYER_NAME)
        
        layer = dataobjects.getObjectFromUri(inputLayer)
        
        
        if WorkContextTracer().isActiveWorkContext():
            workContextName = WorkContextTracer().activeWorkContext()
        else:
            raise GeoAlgorithmExecutionException(self.tr('No work context available.')) 
        
        layerTreeName = workContextName + u' - ' + layerName
        groupName = self.tr(u'Plan - %s') % workContextName
        
        
        root = QgsProject.instance().layerTreeRoot()    
        master = root.findGroup(self.MASTER_GRP_NAME)
        
        if master is None: 
            master = root.insertGroup(0, self.MASTER_GRP_NAME)
        
        group = root.findGroup(groupName)
        if group is None:
            group = master.addGroup(groupName)
 
        QgsMapLayerRegistry.instance().addMapLayer(layer, False)
        addedLayer = group.addLayer(layer)
        addedLayer.setName(layerTreeName)
