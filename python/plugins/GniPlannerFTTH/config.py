# -*- coding: utf-8 -*-

DEMAND_FIELD_NAME = 'demand'
DEMAND_TYPE_FIELD_NAME = 'demandtype'
DEMAND_GROUP_FIELD_NAME = 'group'

BASIC_DEMAND_TYPE = 'basic'
AGGREGATED_DEMAND_TYPE = 'aggregated'
DEMAND_TYPES = [BASIC_DEMAND_TYPE, AGGREGATED_DEMAND_TYPE]

GRAPH_ID_FIELD_NAME = 'id'
GRAPH_COST_FACTOR_FIELD_NAME = 'costfactor'

NODE_ID_FIELD_NAME = 'id'
NODE_PARENT_FIELD_NAME = 'parent_id'
NODE_DEMAND_FIELD_NAME = 'demand'
NODE_TYPE_FIELD_NAME = 'type'
NODE_GROUP_FIELD_NAME = 'group'
NODE_DEM_PNT_COUNT_FIELD_NAME = 'dem_points'

CABLE_ID_FIELD_NAME = 'id'

NODE_TYPE_OLT = 'OLT'
NODE_TYPE_FINAL = 'FINAL'
NODE_TYPE_BRANCHING = 'BRANCHING'

MEAN_COORDINATE_FIELD_NAME = 'UID'
