# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniIntegration
                                 A QGIS plugin
 GniIntegration
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4.QtCore import QSettings
from qgis.core import QgsVectorFileWriter, QgsFeature

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterVector
from processing.core.outputs import OutputVector
from processing.tools import dataobjects, vector

from gni.util.layer import getLayerByTableName

class GenerateDataFromLMS(GeoAlgorithm):

    NETDEVICES_LAYER = 'NETDEVICES_LAYER'
    NETNODES_LAYER = 'NETNODES_LAYER'

    LMS_NAME = 'lms_name'
    GNI_TABLE = 'gni_table'
    IS_VALID = 'is_valid'

    ID_ATTRIBUTE = 'id'
    DESIDNATION = 'oznaczenie'
    
    SUCCESS = 'SUCCESS'
    FAILURE = 'FAILURE'
    
    nodeIntegrationFields = ['status', 'model', 'oznaczenie', 'uwagi', 'data_utworzenia', 'struktura_id', 'typ_struktury']
    structureIntegrationFields = ['status', 'oznaczenie', 'data_utworzenia', 'lokalizacja_id']

    def defineCharacteristics(self):

        # The name that the user will see in the toolbox
        self.name = self.tr('Generate data from LMS')

        # The branch of the toolbox under which the algorithm will appear
        self.group = self.tr('LMS')

        # Parameters        
        self.addParameter(ParameterVector(self.NETNODES_LAYER,
            self.tr('netnodes'), [ParameterVector.VECTOR_TYPE_POINT], True))
        
        self.addParameter(ParameterVector(self.NETDEVICES_LAYER,
            self.tr('netdevices'), [ParameterVector.VECTOR_TYPE_POINT], True))


    def processAlgorithm(self, progress):
        
        self.mappingLayer = getLayerByTableName('lms_integration_mapping')
        self.mappingProvider = self.mappingLayer.dataProvider()
     
        self.generateStructures(progress)
     
        self.generateNodes(progress)         
        
    def generateStructures(self, progress):  
                        
        netNodesLayerFile = self.getParameterValue(self.NETNODES_LAYER)
        
        if netNodesLayerFile is None:
            return
        
        progress.setInfo(self.tr('Generating structures'))
        
        netNodesLayer = dataobjects.getObjectFromUri(netNodesLayerFile)
        netNodes = vector.features(netNodesLayer)
        netNodesCount = len(netNodes)

        current = 0
        for netNode in netNodes:
            
            if netNode.attribute(self.IS_VALID) == 0:
                self.logFailure(progress, "netnodes", netNode, self.tr("invalid data"))
                continue
            
            gnitable = netNode.attribute(self.GNI_TABLE)
            gniStructuresLayer = getLayerByTableName(gnitable)
            gniStructuresProvider = gniStructuresLayer.dataProvider()
            gniStructuresProvider.clearErrors()
            
            structure = QgsFeature()
            structure.setFields(gniStructuresLayer.fields())
            
            for field in self.structureIntegrationFields:
                structure.setAttribute(field, netNode.attribute(field))
            
            structureId = gniStructuresProvider.defaultValue(gniStructuresProvider.fieldNameIndex(self.ID_ATTRIBUTE))
            structure.setAttribute(self.ID_ATTRIBUTE, structureId)
            structure.setGeometry(netNode.geometry())
            
            (retStatus, feature) = gniStructuresProvider.addFeatures([structure])
            
            if retStatus:
                self.logSucces(progress, "netnodes", netNode, gnitable, structure)
            else:
                self.logFailure(progress, "netnodes", netNode, gniStructuresProvider.errors()[0])
                gniStructuresProvider.clearErrors()

            
            progress.setPercentage(float(current) / netNodesCount * 100)
            current = current + 1


    def generateNodes(self, progress):
        
        netDevicesLayerFile = self.getParameterValue(self.NETDEVICES_LAYER)
        
        if netDevicesLayerFile is None:
            return
        
        progress.setInfo(self.tr('Generating nodes'))
        
        netDevicesLayer = dataobjects.getObjectFromUri(netDevicesLayerFile)
        netDevices = vector.features(netDevicesLayer)
        netDevicesCount = len(netDevices)
                
        gniNodesLayer = getLayerByTableName('wezel')
        gniNodesProvider = gniNodesLayer.dataProvider()
        gniNodesProvider.clearErrors()  
        
        gniNodeIdIndex = gniNodesProvider.fieldNameIndex(self.ID_ATTRIBUTE)
        
        current = 0
        for netDevice in netDevices:
            
            if netDevice.attribute(self.IS_VALID) == 0:
                self.logFailure(progress, "netdevices", netDevice, self.tr("invalid data"))
                continue
            
            node = QgsFeature()
            node.setFields(gniNodesLayer.fields())
            
            for field in self.nodeIntegrationFields:
                node.setAttribute(field, netDevice.attribute(field))
            
            node.setAttribute(self.ID_ATTRIBUTE, gniNodesProvider.defaultValue(gniNodeIdIndex))
            node.setGeometry(netDevice.geometry())
                        
            (retStatus, feature) = gniNodesProvider.addFeatures([node])
            
            if retStatus:
                self.logSucces(progress, "netdevices", netDevice, "wezel", node)
            else:
                self.logFailure(progress, "netdevices", netDevice, gniNodesProvider.errors()[0])
                gniNodesProvider.clearErrors()

            progress.setPercentage(float(current) / netDevicesCount * 100)
            current = current + 1

    def logIntegrationStatus(self, attributes):
                
        feature = QgsFeature()
        
        feature.setAttributes([self.mappingProvider.defaultValue(0)] + attributes)
        
        self.mappingProvider.addFeatures([feature])
    
    def logSucces(self, progress, lsmTable, lmsObject, gniTable, gniObject):
        
        progress.setInfo(lsmTable + " " + lmsObject.attribute(self.LMS_NAME))
        
        feature = QgsFeature()
        
        feature.setAttributes([self.mappingProvider.defaultValue(0), \
                               lsmTable, \
                               lmsObject.attribute(self.ID_ATTRIBUTE), lmsObject.attribute(self.LMS_NAME), \
                               gniTable, \
                               gniObject.attribute(self.ID_ATTRIBUTE), gniObject.attribute(self.DESIDNATION), \
                               self.SUCCESS, None])
        
        self.mappingProvider.addFeatures([feature])
        
    def logFailure(self, progress, lsmTable, lmsObject, message):
        
        progress.setInfo(lsmTable + " " + lmsObject.attribute(self.LMS_NAME))
        progress.setInfo(message)
        
        feature = QgsFeature()
        
        feature.setAttributes([self.mappingProvider.defaultValue(0), \
                               lsmTable, \
                               lmsObject.attribute(self.ID_ATTRIBUTE), lmsObject.attribute(self.LMS_NAME), \
                               None, None, None, \
                               self.FAILURE, message])
        
        self.mappingProvider.addFeatures([feature])
