# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.GeoAlgorithmExecutionException import GeoAlgorithmExecutionException

from WorkContextTracer.workcontexttracer import WorkContextTracer

class CheckWorkContext(GeoAlgorithm):

    
    def defineCharacteristics(self):

        self.name = self.tr('Check work context')

        self.group = self.tr('UTIL')

    def processAlgorithm(self, progress):
        
        if not WorkContextTracer().isActiveWorkContext():
            raise GeoAlgorithmExecutionException(self.tr('No work context available.')) 
        