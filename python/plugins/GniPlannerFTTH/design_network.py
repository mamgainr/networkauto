# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GniPlannerFTTH
                                 A QGIS plugin
 GniPlannerFTTH
                              -------------------
        begin                : 2017-02-15
        copyright            : (C) 2017 by Softelnet
        email                : info@softelnet.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4.QtCore import QPyNullVariant  #@UnresolvedImport

from qgis.core import QgsFeature, QgsFeatureRequest, QgsGeometry

from processing.core.GeoAlgorithm import GeoAlgorithm
from processing.core.parameters import ParameterVector, ParameterTableField
from processing.tools import dataobjects
from processing.core.GeoAlgorithmExecutionException import GeoAlgorithmExecutionException

from qgis.networkanalysis import QgsLineVectorLayerDirector, QgsGraphAnalyzer, QgsGraphBuilder

from qgis.networkanalysis import QgsDistanceArcProperter
from utility import getLayerByTableName, resolveLayerByTableName

import config as cf

from WorkContextTracer.workcontexttracer import WorkContextTracer


class DesignNetwork(GeoAlgorithm):

    POTENTIAL_NODES_LAYER = 'POTENTIAL_NODES_LAYER'
    CABLE_ROUTE_LAYER = 'CABLE_ROUTE_LAYER'
    DEMAND_LAYER = 'DEMAND_LAYER'
    DEMAND_ID_FIELD = 'DEMAND_ID_FIELD'
    DEMAND_ADDRESS_FIELD = 'DEMAND_ADDRESS_FIELD'

    def defineCharacteristics(self):

        self.name = self.tr('Design network')

        self.group = self.tr('MAIN')

    
        self.addParameter(ParameterVector(self.POTENTIAL_NODES_LAYER,
            self.tr('Potential nodes'), [ParameterVector.VECTOR_TYPE_POINT], False))

        self.addParameter(ParameterVector(self.CABLE_ROUTE_LAYER,
            self.tr('Cable route'), [ParameterVector.VECTOR_TYPE_LINE], False))

        self.addParameter(ParameterVector(self.DEMAND_LAYER,
            self.tr('Demand points'), [ParameterVector.VECTOR_TYPE_POINT], False))

        self.addParameter(ParameterTableField(self.DEMAND_ID_FIELD,
            self.tr('Demand id field name'), self.DEMAND_LAYER, -1, True))
        
        self.addParameter(ParameterTableField(self.DEMAND_ADDRESS_FIELD,
            self.tr('Demand address field name'), self.DEMAND_LAYER, -1, True))

    def processAlgorithm(self, progress):
        
        potentialNodesLayerFile = self.getParameterValue(self.POTENTIAL_NODES_LAYER)
        cableRouteLayerFile = self.getParameterValue(self.CABLE_ROUTE_LAYER)
        demandLayerFile = self.getParameterValue(self.DEMAND_LAYER)
        
        self.potentialNodesLayer = dataobjects.getObjectFromUri(potentialNodesLayerFile)
        self.cableRouteLayer = dataobjects.getObjectFromUri(cableRouteLayerFile)
        self.demandLayer = dataobjects.getObjectFromUri(demandLayerFile)

        self.demandIdField = self.getParameterValue(self.DEMAND_ID_FIELD)
        self.demandAddressField = self.getParameterValue(self.DEMAND_ADDRESS_FIELD)

        if WorkContextTracer().isActiveWorkContext():
            self.workContextName = WorkContextTracer().activeWorkContext()
        else:
            raise GeoAlgorithmExecutionException(self.tr('No work context available.')) 


        self.pricingLine = resolveLayerByTableName('pricing_line')      
        self.pricingLineProvider = self.pricingLine.dataProvider()
        
        self.pricingPoint = resolveLayerByTableName('pricing_point')
        self.pricingPointProvider = self.pricingPoint.dataProvider()
                
        
        (self.pathLayer, self.pathLayerProvider) = self.resolveCostLayerByTableName(progress, 'path')
        self.paths = []
            
        (self.pathSegmentLayer, self.pathSegmentLayerProvider) = self.resolveCostLayerByTableName(progress, 'path_segment')
        self.pathsSegment = []

        (self.pathPathSegmentLayer, self.pathPathSegmentLayerProvider) = self.resolveCostLayerByTableName(progress, 'path_path_segment')
        self.pathPathSegment = []

        (self.demandDetailLayer, self.demandDetailLayerProvider) = self.resolveCostLayerByTableName(progress, 'demand_details')        
        
        self.versionNumber = self.getNextVersionNumber()
        
        self.pricingPointByNode = {}
        
        self.addedSplices = []
        self.pricingLineResult = []
        self.legCount = {}
        self.legHH = {}
        self.legDemand = {}
        self.edgeCapacity = {}

        potentialNodes = list(self.potentialNodesLayer.getFeatures())
        
        progress.setInfo('Creating graph...')
        builder = QgsGraphBuilder(self.cableRouteLayer.crs())
        #director = QgsLineVectorLayerDirector(self.cableRouteLayer, 3, 'yes', 'no', 'both', 3)
        director = QgsLineVectorLayerDirector(self.cableRouteLayer, -1, '', '', '', 3)
        director.addProperter(QgsDistanceArcProperter())
        director.makeGraph(builder, [f.geometry().asPoint() for f in potentialNodes])
        self.graph = builder.graph()
        

        self.nodeByVertexIndex = {}
        self.vertexIndexByNodeId = {}
                        
        progress.setInfo('Data preparation (initialization)...')
        total = 100.0 / self.graph.vertexCount()
        for vertexIndex in range(self.graph.vertexCount()):
            self.prepareFeatures(self.graph, potentialNodes, vertexIndex)
            progress.setPercentage(int(vertexIndex * total))

        
        progress.setInfo(self.tr('Creating pricing points...'))
        
        progress.setPercentage(0)
        self.findPaths(progress, self.getOltId(potentialNodes))
        
        progress.setPercentage(50)
        for path in self.paths:
            self.connectSplices(self.vertexIndexByNodeId[path[0]])

                    
        progress.setInfo(self.tr('Creating pricing lines...'))
        
        total = 100.0 / len(self.pricingLineResult)
        for current, transmedia in enumerate(self.pricingLineResult):
            transmedias = self.getTranmedias(transmedia)
            for addTransmedia in transmedias:
                ret = self.pricingLineProvider.addFeatures([addTransmedia])
                self.pricingLine.getFeatures(QgsFeatureRequest(ret[1][0].id())).nextFeature(addTransmedia)
                WorkContextTracer().addWorkContextItems(self.pricingLine.id(), [addTransmedia.attribute('id')], "ADD")
            progress.setPercentage(int(current * total))
            
        progress.setInfo(self.tr('Filling paths details...'))   
        self.fillPathDetails(progress)
        
        progress.setInfo(self.tr('Filling demand details...'))   
        self.fillDemandDetails(progress, potentialNodes)
            
    def getEdgeKey(self, id1, id2):
        return str(id1) + "_" + str(id2)
    
    def resolveCostLayerByTableName(self, progress, table):
        layer = getLayerByTableName(table)
        if layer is None:
            progress.setInfo(self.tr(u'Missing layer {0}. Paths costs will not be saved.'.format(table)))
            return (None, None)
        else:
            return (layer, layer.dataProvider())

    def getNextVersionNumber(self):

        exp = u"{0} = '{1}'".format('work_context', self.workContextName)
        result = self.pricingLineProvider.getFeatures(QgsFeatureRequest().setFilterExpression(exp))
        resultList = list(result)
        if len(resultList) == 0:
            return 1
        else:
            maxVersion = int(max(int(i.attribute('version')) for i in resultList))
            return maxVersion + 1
    
    def prepareFeatures(self, g, potentialNodes, vertexIndex):
        vertex = g.vertex(vertexIndex)
        qgspoint = vertex.point()
        for f in potentialNodes:
            if f.geometry().asPoint() == qgspoint:
                self.nodeByVertexIndex[vertexIndex] = f
                self.vertexIndexByNodeId[f.attribute(cf.NODE_ID_FIELD_NAME)] = vertexIndex
                    
    def getOltId(self, potentialNodes):

        for node in potentialNodes:
            if node.attribute(cf.NODE_TYPE_FIELD_NAME) == cf.NODE_TYPE_OLT:
                return node.attribute(cf.NODE_ID_FIELD_NAME)
        
        raise GeoAlgorithmExecutionException(self.tr(u'Missing OLT node.'))
            
    def findPaths(self, progress, oltNodeId):

        nodeParent = {}
        oltVertexIndex = self.vertexIndexByNodeId[oltNodeId]
        
        for vertexIndex, feature in self.nodeByVertexIndex.iteritems():
            parentId = feature.attribute(cf.NODE_PARENT_FIELD_NAME)

            if type(parentId) == QPyNullVariant or parentId is None:
                pathPoints = self.getRoutePointsFromDikstra(oltVertexIndex, vertexIndex)

                if len(pathPoints) > 0:
                    nodeParent[feature.attribute(cf.NODE_ID_FIELD_NAME)] = self.findParent(feature, pathPoints)
            else:
                nodeParent[feature.attribute(cf.NODE_ID_FIELD_NAME)] = parentId


        for feature in self.nodeByVertexIndex.values():

            try:

                if feature.attribute(cf.NODE_TYPE_FIELD_NAME) == cf.NODE_TYPE_FINAL:
                    
                    finalNodeHH = feature.attribute(cf.NODE_DEM_PNT_COUNT_FIELD_NAME)
                    finalNodeDemand = feature.attribute(cf.NODE_DEMAND_FIELD_NAME)
                    oid = feature.attribute(cf.NODE_ID_FIELD_NAME)
                    path = [oid]
                    
                    while oid != oltNodeId:
                        
                        parentId = nodeParent[oid]
                        
                        if oid == parentId:
                            break
                        
                        self.countEdgeCrossing(oid, parentId)
                        self.countEdgeHousehold(oid, parentId, finalNodeHH)
                        self.countEdgeDemand(oid, parentId, finalNodeDemand)
                        path.append(parentId)
                        
                        oid = parentId
                        
                    self.paths.append(path)
            
            except KeyError as ex:
                progress.setInfo(self.tr(u'Missing node {0} parent. Node will not be created '.format(ex)))
                
 
    def findParent(self, feature, route):
        for point in route[1:]:
            vertexIndex = self.graph.findVertex(point)
            if vertexIndex not in self.nodeByVertexIndex:
                continue
            parentNode = self.nodeByVertexIndex[vertexIndex]
            
            parentId = parentNode.attribute(cf.NODE_ID_FIELD_NAME)
            feature.setAttribute(cf.NODE_PARENT_FIELD_NAME, parentId)

            return parentId
         
    def countEdgeCrossing(self, id1, id2):  #TODO: !
        key = self.getEdgeKey(id1, id2)
        if key not in self.legCount:
            self.legCount[key] = 0
        self.legCount[key] = self.legCount[key] + 1
    
    def countEdgeHousehold(self, id1, id2, edgeHH):  #TODO: !
        key = self.getEdgeKey(id1, id2)
        if key not in self.legHH:
            self.legHH[key] = 0

        if edgeHH is not None and type(edgeHH) <> QPyNullVariant:
            self.legHH[key] = self.legHH[key] + edgeHH
            
    def countEdgeDemand(self, id1, id2, edgeDemand):  #TODO: !
        key = self.getEdgeKey(id1, id2)
        if key not in self.legDemand:
            self.legDemand[key] = 0

        if edgeDemand is not None and type(edgeDemand) <> QPyNullVariant:
            self.legDemand[key] = self.legDemand[key] + edgeDemand
    
    def connectSplices(self, spliceIdx):
        
        featureSplice = self.nodeByVertexIndex[spliceIdx]
        parentId = featureSplice.attribute(cf.NODE_PARENT_FIELD_NAME)

        if parentId is not None and type(parentId) != QPyNullVariant:
            parentIdx = self.vertexIndexByNodeId[parentId]

            self.makeConnection(parentIdx, spliceIdx)
            self.connectSplices(parentIdx)
    
    def makeConnection(self, fromId, toId):
  
        if fromId not in self.addedSplices:
            self.createPricingPoint(fromId)
        if toId not in self.addedSplices:
            self.createPricingPoint(toId)
        
        routePoints = list(reversed(self.getRoutePointsFromDikstra(fromId, toId)))  #TODO: obrocic argumenty zamiast calej listy?
        updatedFeature = False
        
        for transmedia in self.pricingLineResult:
            if transmedia.geometry().equals(QgsGeometry.fromPolyline(routePoints)):
                updatedFeature = True
        if not updatedFeature:
            self.createPricingLine(fromId, toId, routePoints)
    
    def createPricingPoint(self, vertexId):
        feature = self.nodeByVertexIndex[vertexId]
        
        splice = QgsFeature()
        splice.setFields(self.pricingPoint.fields())
        splice.setGeometry(feature.geometry())
        
        splice.setAttribute('type', feature.attribute(cf.NODE_TYPE_FIELD_NAME))
        splice.setAttribute('version', self.versionNumber)
        splice.setAttribute('work_context', self.workContextName)
        splice.setAttribute('id', self.pricingPointProvider.defaultValue(0))
        
        ret = self.pricingPointProvider.addFeatures([splice])
        addSpl = ret[1]
        addedSpliceFeature = addSpl[0]
        self.addedSplices.append(vertexId)
  
        self.pricingPoint.getFeatures(QgsFeatureRequest(addedSpliceFeature.id())).nextFeature(splice)
        WorkContextTracer().addWorkContextItems(self.pricingPoint.id(), [splice.attribute('id')], "ADD")
        
        self.pricingPointByNode[feature.attribute(cf.NODE_ID_FIELD_NAME)] = splice.attribute('id')

    def createPricingLine(self, fromId, toId, routePoints):

        if routePoints:
            id1 = self.nodeByVertexIndex[fromId].attribute(cf.NODE_ID_FIELD_NAME)
            id2 = self.nodeByVertexIndex[toId].attribute(cf.NODE_ID_FIELD_NAME)
            key = self.getEdgeKey(id2, id1)
            
            demand = self.legDemand[key]
            
            fet = QgsFeature()
            fet.setFields(self.pricingLine.fields())

            geom = QgsGeometry.fromPolyline(routePoints)
            fet.setGeometry(geom)
                        
            fet.setAttribute('version', self.versionNumber)
            fet.setAttribute('work_context', self.workContextName)
            fet.setAttribute('profile', str(demand))
            fet.setAttribute('length', geom.length())
            fet.setAttribute('pricing_point_from_id', self.pricingPointByNode[id1])
            fet.setAttribute('pricing_point_to_id', self.pricingPointByNode[id2])
            
            self.pricingLineResult.append(fet)
            
            
            self.edgeCapacity[key] = self.getProfile(demand)
    
    def getRoutePointsFromDikstra(self, fromId, toId):  #TODO: przepisac aby zwaracla wiezcholki lub ich indeksy
        routePoints = []
        from_point = self.graph.vertex(fromId).point()
        (tree, _) = QgsGraphAnalyzer.dijkstra(self.graph, fromId, 0)
        if tree[toId] == -1:
            pass
        else:
            curPos = toId 
            while (curPos != fromId):
                routePoints.append(self.graph.vertex(self.graph.arc(tree[ curPos ]).inVertex()).point())
                curPos = self.graph.arc(tree[ curPos ]).outVertex()
    
            routePoints.append(from_point)
        return routePoints
    
    def getTranmedias(self, transmedia):
        transmedias = []
        demand = int(transmedia.attribute('profile'))
        if demand < 289:
            profile = self.getProfile(demand)
            transmedia.setAttribute('profile', str(profile))
            transmedia.setAttribute('id', self.pricingLineProvider.defaultValue(0))

            transmedias.append(transmedia)
        else:
            (numOfCables, rest) = divmod(demand, 288)
            if rest > 0:
                profile = self.getProfile(rest)
                newTransmedia = QgsFeature(transmedia)
                newTransmedia.setAttribute('id', self.pricingLineProvider.defaultValue(0))
                newTransmedia.setAttribute('profile', str(profile))
                transmedias.append(newTransmedia)
                
            profile = self.getProfile(demand)
            transmedia.setAttribute('profile', str(profile))  
            for _ in range(numOfCables):
                newTransmedia = QgsFeature(transmedia)
                newTransmedia.setAttribute('id', self.pricingLineProvider.defaultValue(0))
                transmedias.append(newTransmedia)
        return transmedias
    
    def getProfile(self, demand):
        if demand <= 7:
            return '12'
        elif demand <= 16:
            return '24'
        elif demand <= 33:
            return '48'
        elif demand <= 50:
            return '72'
        elif demand <= 100:
            return '144'
        elif demand > 100:
            return '288'
    
    def fillPathDetails(self, progress):
        if self.pathLayer is None or self.pathSegmentLayer is None or self.pathPathSegmentLayer is None:
            return
        
        self.pathPathSegmentLayer.startEditing()
        
        self.savedpaths = []
        
        total = 100.0 / len(self.paths)
        for current, path in enumerate(self.paths):
            finalNode = path[0]
            lastPoint = -1
            for point in reversed(path):
                    
                pricingPointId = self.pricingPointByNode[finalNode]
                if not pricingPointId > -1:
                    continue
                else:
                    if pricingPointId not in self.savedpaths:
                        (pathDemand, pathHH) = self.getDemandHHFromNode(finalNode)
                        pathId = self.createPath(pricingPointId, pathDemand, pathHH) 
                if lastPoint == -1:
                    lastPoint = point
                    continue
                
                pricingPointFromId = self.pricingPointByNode[int(lastPoint)]
                if not pricingPointFromId > -1:
                    continue
                
                key = self.getEdgeKey(point, lastPoint)
                if key not in self.legCount:
                    costPercent = -1
                else:
                    costPercent = (1 / float(self.legCount[key])) * 100
                    
                pricingPointToId = self.pricingPointByNode[int(point)]
                
                edgeDemand = 0
                
                if key in self.legHH:
                    edgeHH = self.legHH[key]
                else:
                    edgeHH = 0
                
                if key in self.legDemand:
                    edgeDemand = self.legDemand[key]
                else:
                    edgeDemand = 0
                
                if key in self.edgeCapacity:
                    edgeCapacityN = self.edgeCapacity[key]
                else:
                    edgeCapacityN = 0
                    
                self.createPathSegment(pathId, pricingPointFromId, pricingPointToId, costPercent, edgeCapacityN, edgeDemand, edgeHH)
                
                lastPoint = point
                
            progress.setPercentage(int(current * total))
        
        self.pathPathSegmentLayer.commitChanges()
    
    def getDemandHHFromNode(self, oid):
        exp = u"{0} = {1}".format(cf.NODE_ID_FIELD_NAME, str(oid))
        request = QgsFeatureRequest().setFilterExpression(exp)
        feature = QgsFeature()
        self.potentialNodesLayer.getFeatures(request).nextFeature(feature)
        if feature.id() == 0:
            return (0, 0)

        demand = feature.attribute(cf.NODE_DEMAND_FIELD_NAME)
        if demand is None or type(demand) == QPyNullVariant:
            demand = 0
        hh = feature.attribute(cf.NODE_DEM_PNT_COUNT_FIELD_NAME)
        if hh is None or type(hh) == QPyNullVariant:
            hh = 0
        
        return (demand, hh)
              
    def createPath(self, pricingPointId, pathDemand, pathHH):
        path = QgsFeature()
        path.setFields(self.pathLayer.fields())
        path.setAttribute('pricing_point_id', round(int(pricingPointId)))
        path.setAttribute('path_demand', round(int(pathDemand)))
        path.setAttribute('path_dem_points', round(int(pathHH)))
        path.setAttribute('work_context', str(self.workContextName))
        path.setAttribute('version', self.versionNumber)
        ret = self.pathLayerProvider.addFeatures([path])
        self.pathLayer.getFeatures(QgsFeatureRequest(ret[1][0].id())).nextFeature(path)
        self.savedpaths.append(pricingPointId)
        return path.attribute('id')
    
    def createPathSegment(self, pathId, fromId, toId, costPercent, edgeCapacity, edgeDemand, edgeHH):
        pathLeg = QgsFeature()
        pathLeg.setFields(self.pathSegmentLayer.fields())
        pathLeg.setAttribute('edge_capacity', edgeCapacity)  #TODO: do usuniecia
        pathLeg.setAttribute('cost_percent', costPercent)
        pathLeg.setAttribute('edge_demand', edgeDemand)
        pathLeg.setAttribute('edge_dem_points', edgeHH)
        pathLeg.setAttribute('pricing_point_from_id', fromId)
        pathLeg.setAttribute('pricing_point_to_id', toId)
        ret = self.pathSegmentLayerProvider.addFeatures([pathLeg])
        self.pathSegmentLayer.getFeatures(QgsFeatureRequest(ret[1][0].id())).nextFeature(pathLeg)
        oid = pathLeg.attribute('id')
        self.createPathPathSegment(pathId, oid)
    
    def createPathPathSegment(self, pathId, pathSegmentId):
        pathLeg = QgsFeature()
        pathLeg.setFields(self.pathPathSegmentLayer.fields())
        pathLeg.setAttribute('path_id', round(int(pathId)))
        pathLeg.setAttribute('path_segment_id', pathSegmentId)
        self.pathPathSegmentLayer.addFeature(pathLeg)
        
    def fillDemandDetails(self, progress, potentialNodes):
        if self.demandDetailLayer is None:
            return
        
        finalNodes = [n for n in potentialNodes if n.attribute(cf.NODE_TYPE_FIELD_NAME) == cf.NODE_TYPE_FINAL]
        
        self.demandDetailLayer.startEditing()
        
        total = 100.0 / len(finalNodes)
        for current, node in enumerate(finalNodes):
            
            if node.attribute(cf.NODE_ID_FIELD_NAME) in self.pricingPointByNode:
            
                exp = u"{0} = '{1}'".format(cf.DEMAND_GROUP_FIELD_NAME, node.attribute(cf.NODE_GROUP_FIELD_NAME))
                for demandPoint in self.demandLayer.getFeatures(QgsFeatureRequest().setFilterExpression(exp)):
    
                    self.addDemandDetail(self.pricingPointByNode[node.attribute(cf.NODE_ID_FIELD_NAME)], demandPoint)
                
            progress.setPercentage(int(current * total))
            
        self.demandDetailLayer.commitChanges()
        
    def addDemandDetail(self, pricPoint, demandPoint):

        if self.demandAddressField is not None:
            detail = demandPoint.attribute(self.demandAddressField)
        else:
            detail = ""
        
        if self.demandIdField is not None:
            demandId = str(demandPoint.attribute(self.demandIdField))
        else:
            demandId = str(0)
        
        feature = QgsFeature()
        feature.setFields(self.demandDetailLayer.fields())
        feature.setAttribute('demand_id', demandId)
        feature.setAttribute('pricing_point_id', pricPoint)
        feature.setAttribute('detail', detail)
        feature.setAttribute('work_context', self.workContextName)
        feature.setAttribute('version', self.versionNumber)
        feature.setAttribute('demand', int(demandPoint.attribute(cf.DEMAND_FIELD_NAME)))
        feature.setAttribute('position', demandPoint.geometry().exportToWkt())

        self.demandDetailLayer.addFeature(feature)
